%get the 15the, the 50th and 85th percentile of all three runs for all
%five datasets with and without fine-tuning being applied

%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

M = get_confusion_matrices_from_txt_files(fullfile(folder_root,'\Data\experiment_logfiles\FT_merged\256'));
data_ft = get_metrics_from_matrices_folders(M);
clear M

% data = data_dcnn_ft;
datasets_ft = fieldnames(data_ft);
data.ft.Ice = [];
data.ft.Droplet = [];



%% Data with fine-tuning
for cnt = 1:length(datasets_ft)
    if ~isfield(data.ft.Ice,'accuracy')
        data.ft.Ice.accuracy = data_ft.(datasets_ft{cnt}).Ice.accuracy;
        data.ft.Ice.FDR = data_ft.(datasets_ft{cnt}).Ice.FDR;
    else
        data.ft.Ice.accuracy = [data.ft.Ice.accuracy;data_ft.(datasets_ft{cnt}).Ice.accuracy];
        data.ft.Ice.FDR = [data.ft.Ice.FDR;data_ft.(datasets_ft{cnt}).Ice.FDR];
    end
    
    if ~isfield(data.ft.Droplet,'accuracy')
        data.ft.Droplet.accuracy = data_ft.(datasets_ft{cnt}).Droplet.accuracy;
        data.ft.Droplet.FDR = data_ft.(datasets_ft{cnt}).Droplet.FDR;
    else
        data.ft.Droplet.accuracy = [data.ft.Droplet.accuracy;data_ft.(datasets_ft{cnt}).Droplet.accuracy];
        data.ft.Droplet.FDR = [data.ft.Droplet.FDR;data_ft.(datasets_ft{cnt}).Droplet.FDR];
    end
end

%Calculate all offsets
for cnt = 1:length(data.ft.Ice.accuracy)
    data.ft.Ice.offset(cnt) = 1-(1-data.ft.Ice.FDR(cnt))*(2-data.ft.Ice.accuracy(cnt));
end
for cnt = 1:length(data.ft.Droplet.accuracy)
    data.ft.Droplet.offset(cnt) = 1-(1-data.ft.Droplet.FDR(cnt))*(2-data.ft.Droplet.accuracy(cnt));
end

%Calculate median offset
data.ft.Ice.median = prctile(data.ft.Ice.offset,50);
data.ft.Ice.errorP = prctile(data.ft.Ice.offset,85);
data.ft.Ice.errorN = prctile(data.ft.Ice.offset,15);
data.ft.Droplet.median = prctile(data.ft.Droplet.offset,50);
data.ft.Droplet.errorP = prctile(data.ft.Droplet.offset,85);
data.ft.Droplet.errorN = prctile(data.ft.Droplet.offset,15);

clear cnt data_ft datasets_ft