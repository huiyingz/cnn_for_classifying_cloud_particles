
%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path


load(fullfile(folder_root,'\Figures\Figure_12\pred_FT.mat'));

datasets = fieldnames(pred_FT);
bins = pred_FT.(datasets{1}).bins;
%Number of bins
N = size(bins,1);

%datasets(end) = []; %Remove SON2017 for mainly liquid
% normal.Ice.accuracy = pred.(datasets{1}).Ice.accuracy;
data.Ice.accuracy = [];
data.Ice.FDR = [];
data.Droplet.accuracy = [];
data.Droplet.FDR = [];

for cnt = 1:length(datasets)
    a = -1;
    for cnt2 = 1:N
        b =  strcat('b',int2str(cnt2));
        %SON2017 hat andere L�nge Only if fine-tuned!!
        if ~isequal(datasets{cnt},'SON2017')
            if ~isnan(pred_FT.(datasets{cnt}).Ice.x(2*cnt2+a))
                if ~isfield(data.Ice.accuracy,(b))
                    data.Ice.accuracy.(b) = (pred_FT.(datasets{cnt}).Ice.accuracy(2*cnt2+a:2*cnt2+a+2));
                    data.Ice.FDR.(b) = (pred_FT.(datasets{cnt}).Ice.FDR(2*cnt2+a:2*cnt2+a+2));
                else
                    data.Ice.accuracy.(b) = [data.Ice.accuracy.(b);...
                        (pred_FT.(datasets{cnt}).Ice.accuracy(2*cnt2+a:2*cnt2+a+2))];
                    data.Ice.FDR.(b) = [data.Ice.FDR.(b);...
                        (pred_FT.(datasets{cnt}).Ice.FDR(2*cnt2+a:2*cnt2+a+2))];
                end
            end
            if ~isnan(pred_FT.(datasets{cnt}).Droplet.x(2*cnt2+a))
                if ~isfield(data.Droplet.accuracy,(b))
                    data.Droplet.accuracy.(b) = (pred_FT.(datasets{cnt}).Droplet.accuracy(2*cnt2+a:2*cnt2+a+2));
                    data.Droplet.FDR.(b) = (pred_FT.(datasets{cnt}).Droplet.FDR(2*cnt2+a:2*cnt2+a+2));
                else
                    data.Droplet.accuracy.(b) = [data.Droplet.accuracy.(b);...
                        (pred_FT.(datasets{cnt}).Droplet.accuracy(2*cnt2+a:2*cnt2+a+2))];
                    data.Droplet.FDR.(b) = [data.Droplet.FDR.(b);...
                        (pred_FT.(datasets{cnt}).Droplet.FDR(2*cnt2+a:2*cnt2+a+2))];
                end
            end
        else
            if ~isnan(pred_FT.(datasets{cnt}).Ice.x(2*cnt2-1))
                if ~isfield(data.Ice.accuracy,(b))
                    data.Ice.accuracy.(b) = (pred_FT.(datasets{cnt}).Ice.accuracy(2*cnt2-1:2*cnt2));
                    data.Ice.FDR.(b) = (pred_FT.(datasets{cnt}).Ice.FDR(2*cnt2-1:2*cnt2));
                else
                    data.Ice.accuracy.(b) = [data.Ice.accuracy.(b);...
                        (pred_FT.(datasets{cnt}).Ice.accuracy(2*cnt2-1:2*cnt2))];
                    data.Ice.FDR.(b) = [data.Ice.FDR.(b);...
                        (pred_FT.(datasets{cnt}).Ice.FDR(2*cnt2-1:2*cnt2))];
                end
            end
            if ~isnan(pred_FT.(datasets{cnt}).Droplet.x(2*cnt2-1))
                if ~isfield(data.Droplet.accuracy,(b))
                    data.Droplet.accuracy.(b) = (pred_FT.(datasets{cnt}).Droplet.accuracy(2*cnt2-1:2*cnt2));
                    data.Droplet.FDR.(b) = (pred_FT.(datasets{cnt}).Droplet.FDR(2*cnt2-1:2*cnt2));
                else
                    data.Droplet.accuracy.(b) = [data.Droplet.accuracy.(b);...
                        (pred_FT.(datasets{cnt}).Droplet.accuracy(2*cnt2-1:2*cnt2))];
                    data.Droplet.FDR.(b) = [data.Droplet.FDR.(b);...
                        (pred_FT.(datasets{cnt}).Droplet.FDR(2*cnt2-1:2*cnt2))];
                end
            end
        end
        a = a+1;
    end
end



for cnt2 = 1:N
    b =  strcat('b',int2str(cnt2));
    if isfield(data.Ice.accuracy,b)
        for cnt = 1:length(data.Ice.accuracy.(b))
            data.Ice.offset.(b)(cnt) = 1-(1-data.Ice.FDR.(b)(cnt))*(2-data.Ice.accuracy.(b)(cnt));
        end
    end
    if isfield(data.Droplet.accuracy,b)
        for cnt = 1:length(data.Droplet.accuracy.(b))
            data.Droplet.offset.(b)(cnt) = 1-(1-data.Droplet.FDR.(b)(cnt))*(2-data.Droplet.accuracy.(b)(cnt));
        end
    end
end

%Calculate all offsets



%Calculate median offset
for cnt2 = 1:N
    b =  strcat('b',int2str(cnt2));
    if isfield(data.Ice.accuracy,b)
        data.Ice.(b).median = median(data.Ice.offset.(b));
        data.Ice.(b).errorP = prctile(data.Ice.offset.(b),85);
        data.Ice.(b).errorN = prctile(data.Ice.offset.(b),15);
        data.Ice.(b).min = (1-max(data.Ice.FDR.(b)))*(2-max(data.Ice.accuracy.(b)));
        data.Ice.(b).max = (1-min(data.Ice.FDR.(b)))*(2-min(data.Ice.accuracy.(b)));
    end
    if isfield(data.Droplet.accuracy,b)
        data.Droplet.(b).median = median(data.Droplet.offset.(b));
        data.Droplet.(b).min = (1-max(data.Droplet.FDR.(b)))*(2-max(data.Droplet.accuracy.(b)));
        data.Droplet.(b).max = (1-min(data.Droplet.FDR.(b)))*(2-min(data.Droplet.accuracy.(b)));
        data.Droplet.(b).errorP = prctile(data.Droplet.offset.(b),85);
        data.Droplet.(b).errorN = prctile(data.Droplet.offset.(b),15);
    end
end

clear a b bins cnt cnt2 datasets folder_root normal pred pred_FT