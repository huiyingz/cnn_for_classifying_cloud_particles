The dataset mat files include the following fields:
prtclID = particle ID to track each single particle (recording time- position in x-axis - position in y-axis - position in z-axis - equivalent-area particle diameter) 
campaign = name of campaign
instrument = name of instrument
classifiedBy = shortcut of person who classified the particles
description = comments on the dataset
metricnames = names of extracted features
metricmat = values of the extracted features
prtclIm = complex images of the particles
class = label set by classifying person. (Particle_nubbly = ice crystal, Particle_round = liquid droplet, Artifact = Artifact)
cpType = class (different type)