Contains the mat files with the results of the predictions on models being trained on four merged datasets (folder "merged") and tested on the fifth and models trained on single datasets and tested on the same or other single datasets ("single_to_single").

The mat files have fields of:
prtclID = particle ID to track each single particle (recording time- position in x-axis - position in y-axis - position in z-axis - equivalent-area particle diameter) 
campaign = name of campaign
instrument = name of instrument
classifiedBy = shortcut of person who classified the particles
description = comments on the dataset
metricnames = names of extracted features
metricmat = values of the extracted features
prtclIm = complex images of the particles
class = label set by classifying person. (Particle_nubbly = ice crystal, Particle_round = liquid droplet, Artifact = Artifact)
cpType = class (different type)
Artifact, Droplet, Ice = contain different fields (explained for artifacts): class (ll number of rows where we have an artifact), predicted (all number of rows where we the particle was predicted as artifact), true (all number of rows where there is an artifact (class) and which was predicted as artifact (predicted), false (all number of rows which were predicted as artifact but ar not), missing (all number of rows which are artifacts but not predicted as artifact), accuracy (per class accuracy of artifacts), FDR (per class FDR of artifacts)
predicted = categorical of the predicted classes
accuracy = overall accuracy



In merged:
Name of the mat files contain name of dataset on which the model was tested on (trained on the other four), the name of the method used (SVM or Tree), the used splitting of the training data (e.g. "90-10" = 90% of the data for training, 10% for validation).

In single_to_single
Name of mat files: Starts with the name of the dataset the model was trained on. The second name is the name of the dataset the model was tested on. The method (SVM or Tree)