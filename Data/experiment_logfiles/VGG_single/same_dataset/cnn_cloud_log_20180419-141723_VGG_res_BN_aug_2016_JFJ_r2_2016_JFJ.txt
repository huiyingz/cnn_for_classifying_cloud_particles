Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132531_VGG_res_BN_aug_2016_JFJ_r2', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_JFJ.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141723_VGG_res_BN_aug_2016_JFJ_r2_2016_JFJ', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_JFJ.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.98338, macro average f1 score: 0.96533
classification report:
             precision    recall  f1-score   support

          0    0.99694   0.98288   0.98986       993
          1    0.98305   0.98305   0.98305       354
          2    0.86486   0.98969   0.92308        97

avg / total    0.98466   0.98338   0.98370      1444
confusion matrix:
[[976   6  11]
 [  2 348   4]
 [  1   0  96]]
