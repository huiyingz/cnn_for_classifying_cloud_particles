Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132529_VGG_res_BN_aug_2016_SON_r1', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_SON.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141702_VGG_res_BN_aug_2016_SON_r1_2016_SON', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_SON.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.96454, macro average f1 score: 0.96306
classification report:
             precision    recall  f1-score   support

          0    0.95698   0.97165   0.96426      1305
          1    0.97524   0.95848   0.96679      1397
          2    0.95370   0.96262   0.95814       428

avg / total    0.96468   0.96454   0.96455      3130
confusion matrix:
[[1268   28    9]
 [  47 1339   11]
 [  10    6  412]]
