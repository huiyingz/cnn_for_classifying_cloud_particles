
fnames = fieldnames(M);
l = length(fnames);
names = strings([l,1]);
for cnt = 1:l
    newStr = erase(fnames{cnt},'_r1');
    newStr = erase(newStr,'_r2');
    newStr = erase(newStr,'_r3');
    names{cnt} = newStr;
end
clear cnt newStr

%X: same number, same string
[D,~,X] = unique(names(:));
K=3;
for cnt = 1:length(D)
    k = find(X==cnt);
    for i = 1:3 %three values for the three different tests
        CM = M.(fnames{k(i)});
        N = sum(sum(CM));
        data.(names{k(1)}).accuracy(i,1) = trace(CM)/N;
        %calculate values to get HSS
        s = 0;
        for j = 1:K
            s = s+sum(CM(:,j))*sum(CM(j,:));
        end
        E = s/(N^2);
        data.(names{k(1)}).HSS(i,1) = (data.(names{k(1)}).accuracy(i,1)-E)/(1-E);
        data.(names{k(1)}).Artifact.accuracy(i,1) = CM(1,1)/sum(CM(1,:));
        data.(names{k(1)}).Artifact.FDR(i,1) = (sum(CM(:,1))-CM(1,1))/sum(CM(:,1));
        data.(names{k(1)}).Droplet.accuracy(i,1) = CM(2,2)/sum(CM(2,:));
        data.(names{k(1)}).Droplet.FDR(i,1) = (sum(CM(:,2))-CM(2,2))/sum(CM(:,2));
        data.(names{k(1)}).Ice.accuracy(i,1) = CM(3,3)/sum(CM(3,:));
        data.(names{k(1)}).Ice.FDR(i,1) = (sum(CM(:,3))-CM(3,3))/sum(CM(:,3));
        data.(names{k(1)}).BER(i,1) = (3-data.(names{k(1)}).Artifact.accuracy(i,1)...
            -data.(names{k(1)}).Droplet.accuracy(i,1)-data.(names{k(1)}).Ice.accuracy(i,1))/3;
    end
end

