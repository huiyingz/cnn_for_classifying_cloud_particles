Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132529_VGG_res_BN_aug_2017_SON_r3', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_SON.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141701_VGG_res_BN_aug_2017_SON_r3_2016_SON', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_SON.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.78466, macro average f1 score: 0.75477
classification report:
             precision    recall  f1-score   support

          0    0.97823   0.61992   0.75891      1305
          1    0.84704   0.89191   0.86890      1397
          2    0.48197   0.93692   0.63651       428

avg / total    0.85182   0.78466   0.79126      3130
confusion matrix:
[[ 809  204  292]
 [  12 1246  139]
 [   6   21  401]]
