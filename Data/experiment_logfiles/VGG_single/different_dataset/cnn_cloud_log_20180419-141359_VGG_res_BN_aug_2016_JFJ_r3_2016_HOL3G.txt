Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132530_VGG_res_BN_aug_2016_JFJ_r3', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_HOL3G.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141359_VGG_res_BN_aug_2016_JFJ_r3_2016_HOL3G', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_HOL3G.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.38972, macro average f1 score: 0.26440
classification report:
             precision    recall  f1-score   support

          0    0.22362   0.97622   0.36389       799
          1    0.97670   0.27511   0.42930      3504
          2    0.00000   0.00000   0.00000       172

avg / total    0.80470   0.38972   0.40112      4475
confusion matrix:
[[ 780   19    0]
 [2540  964    0]
 [ 168    4    0]]
