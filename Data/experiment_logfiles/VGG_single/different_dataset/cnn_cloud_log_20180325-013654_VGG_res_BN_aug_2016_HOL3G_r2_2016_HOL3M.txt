Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180324-185043_VGG_res_BN_aug_2016_HOL3G_r2', no_phase=False, pred_dir='predictions', pred_set='prep_data/dataset_2016_HOL3M.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180325-013654_VGG_res_BN_aug_2016_HOL3G_r2_2016_HOL3M', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/dataset_2016_HOL3M.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.81139, macro average f1 score: 0.55216
classification report:
             precision    recall  f1-score   support

          0    0.81002   0.99051   0.89121     15694
          1    0.87093   0.17979   0.29805      4166
          2    0.68812   0.35369   0.46723       393

avg / total    0.82018   0.81139   0.76097     20253
confusion matrix:
[[15545   108    41]
 [ 3395   749    22]
 [  251     3   139]]
