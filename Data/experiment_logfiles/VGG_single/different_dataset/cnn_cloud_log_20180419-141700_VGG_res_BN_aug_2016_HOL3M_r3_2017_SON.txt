Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132530_VGG_res_BN_aug_2016_HOL3M_r3', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2017_SON.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141700_VGG_res_BN_aug_2016_HOL3M_r3_2017_SON', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2017_SON.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.69936, macro average f1 score: 0.59542
classification report:
             precision    recall  f1-score   support

          0    0.24368   1.00000   0.39187       376
          1    0.80000   0.47059   0.59259        34
          2    1.00000   0.66915   0.80179      3485

avg / total    0.92524   0.69936   0.76039      3895
confusion matrix:
[[ 376    0    0]
 [  18   16    0]
 [1149    4 2332]]
