data = load('Single_to_different_FT.mat');
data = data.data;
fnames = fieldnames(data);
l = length(fnames);



table = cell(l+5,10);
table(3:l+2,1) = fnames;
table(2,2) = {'Accuracy'};
table(2,3) = {'BER'};
table(2,4) = {'HSS'};
table(1,5) = {'Water'};
table(2,5) = {'Accuracy'};
table(2,6) = {'FDR'};
table(1,7) = {'Ice'};
table(2,7) = {'Accuracy'};
table(2,8) = {'FDR'};
table(1,9) = {'Artifact'};
table(2,9) = {'Accuracy'};
table(2,10) = {'FDR'};
table(l+4,1) = {'Mean'};
table(l+5,1) = {'Standard deviation'};
table(l+6,1) = {'Standard error'};


for i = 1:l %Number of datasets
    accuracy(i,1) = mean(data.(fnames{i}).accuracy);
    table(i+2,2) = {accuracy(i,1)};
    BER(i,1) = mean(data.(fnames{i}).BER);
    table(i+2,3) = {BER(i,1)};
    HSS(i,1) = mean(data.(fnames{i}).HSS);
    table(i+2,4) = {HSS(i,1)};
    Droplet.accuracy(i,1) = mean(data.(fnames{i}).Droplet.accuracy);
    table(i+2,5) = {Droplet.accuracy(i,1)};
    Droplet.FDR(i,1) = mean(data.(fnames{i}).Droplet.FDR);
    if isnan(Droplet.FDR(i,1))
        Droplet.FDR(i,1) = 0;
    end
    table(i+2,6) = {Droplet.FDR(i,1)};
    Ice.accuracy(i,1) = mean(data.(fnames{i}).Ice.accuracy);
    table(i+2,7) = {Ice.accuracy(i,1)};
    Ice.FDR(i,1) = mean(data.(fnames{i}).Ice.FDR);
    if isnan(Ice.FDR(i,1))
        Ice.FDR(i,1) = 0;
    end
    table(i+2,8) = {Ice.FDR(i,1)};
    Artifact.accuracy(i,1) = mean(data.(fnames{i}).Artifact.accuracy);
    table(i+2,9) = {Artifact.accuracy(i,1)};
    Artifact.FDR(i,1) = mean(data.(fnames{i}).Artifact.FDR);
    if isnan(Artifact.FDR(i,1))
        Artifact.FDR(i,1) = 0;
    end
    table(i+2,10) = {Artifact.FDR(i,1)};
end

table(l+4,2) = {mean(accuracy)};
table(l+5,2) = {std(accuracy)};
table(l+6,2) = {std(accuracy)/sqrt(20)};
table(l+4,3) = {mean(BER)};
table(l+5,3) = {std(BER)};
table(l+6,3) = {std(BER)/sqrt(20)};
table(l+4,4) = {mean(HSS)};
table(l+5,4) = {std(HSS)};
table(l+6,4) = {std(HSS)/sqrt(20)};
table(l+4,5) = {mean(Droplet.accuracy)};
table(l+5,5) = {std(Droplet.accuracy)};
table(l+6,5) = {std(Droplet.accuracy)/sqrt(20)};
table(l+4,6) = {mean(Droplet.FDR)};
table(l+5,6) = {std(Droplet.FDR)};
table(l+6,6) = {std(Droplet.FDR)/sqrt(20)};
table(l+4,7) = {mean(Ice.accuracy)};
table(l+5,7) = {std(Ice.accuracy)};
table(l+6,7) = {std(Ice.accuracy)/sqrt(20)};
table(l+4,8) = {mean(Ice.FDR)};
table(l+5,8) = {std(Ice.FDR)};
table(l+6,8) = {std(Ice.FDR)/sqrt(20)};
table(l+4,9) = {mean(Artifact.accuracy)};
table(l+5,9) = {std(Artifact.accuracy)};
table(l+6,9) = {std(Artifact.accuracy)/sqrt(20)};
table(l+4,10) = {mean(Artifact.FDR)};
table(l+5,10) = {std(Artifact.FDR)};
table(l+6,10) = {std(Artifact.FDR)/sqrt(20)};



xlswrite('SingleToSingleDatasets',table)