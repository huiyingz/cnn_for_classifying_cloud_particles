filelist = dir('*.txt');
filenames = {filelist.name};
l = length(filelist);

for cnt = 1:l
    data = textread(filenames{cnt}, '%s');
    a = strfind(filenames{cnt},'aug_');
    name = filenames{cnt}(a:length(filenames{cnt})-4);
    s = length(data);
    a = find(strcmp(data,'matrix:'));
    data_new = data(a+1:length(data));
    

    data_num = zeros(9,1);
    z =9;
    for i = length(data_new):-1:1
        a = strfind(data_new{i},'[');
        b = strfind(data_new{i},']');
        if ~isempty(a)
            data_new{i}(a) = [];
        end
        if ~isempty(b)
            data_new{i}(b) = [];
        end
        if isempty(data_new{i})
            data_new(i) =[];
        else
            data_num(z) = str2num(data_new{i});
            z = z-1;
        end
    end  
    M.(name) = vec2mat(data_num,3);
end
clear a b cnt data data_new data_num filelist filenames i l name s z