Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180324-185043_VGG_res_BN_aug_2016_HOL3G_r3', no_phase=False, pred_dir='predictions', pred_set='prep_data/dataset_2016_SON.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180325-013621_VGG_res_BN_aug_2016_HOL3G_r3_2016_SON', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/dataset_2016_SON.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.86727, macro average f1 score: 0.85820
classification report:
             precision    recall  f1-score   support

          0    0.81410   0.95797   0.88020      6377
          1    0.94619   0.79989   0.86691      7056
          2    0.83433   0.82077   0.82749      2215

avg / total    0.87652   0.86727   0.86674     15648
confusion matrix:
[[6109  203   65]
 [1116 5644  296]
 [ 279  118 1818]]
