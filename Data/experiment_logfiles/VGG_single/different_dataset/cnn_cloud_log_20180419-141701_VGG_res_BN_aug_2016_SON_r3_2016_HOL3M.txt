Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models/cnn_cloud_model_20180419-132530_VGG_res_BN_aug_2016_SON_r3', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_HOL3M.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180419-141701_VGG_res_BN_aug_2016_SON_r3_2016_HOL3M', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_HOL3M.pkl', train=False, train_set='prep_data/train_classified_32.pkl', use_class=None, use_shape=False, val=False, val_set='prep_data/val_classified_32.pkl', weigh_volume=False)

test set evaluation
accuracy: 0.79733, macro average f1 score: 0.49832
classification report:
             precision    recall  f1-score   support

          0    0.79803   0.99653   0.88630      3168
          1    0.80851   0.04774   0.09015       796
          2    0.72917   0.40230   0.51852        87

avg / total    0.79861   0.79733   0.72196      4051
confusion matrix:
[[3157    8    3]
 [ 748   38   10]
 [  51    1   35]]
