Arguments: Namespace(arch='VGG', augment=True, batch_size=256, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=10, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.001, log_dir='logfiles', max_epochs=200, model_dir='models', no_phase=False, pred_dir='predictions', pred_set='prep_data/dataset_2016_HOL3G.pkl', predict=True, recording_steps=300, residual=True, resume_training=False, sigma=100.0, suffix='20180325-015723_VGG_res_BN_aug_merged_2016_HOL3G_r2', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/dataset_2016_HOL3G.pkl', train=True, train_set='prep_data/train_merged_2016_HOL3G.pkl', use_class=None, use_shape=False, val=True, val_set='prep_data/val_merged_2016_HOL3G.pkl', weigh_volume=False)

epoch: 1, step: 221
training loss: 0.43415, validation loss: 9.03929
accuracy: 0.65479, macro average f1 score: 0.55236
classification report:
             precision    recall  f1-score   support

          0    0.58391   0.97372   0.73004      2930
          1    0.90630   0.93223   0.91908      1328
          2    1.00000   0.00400   0.00796      2002

avg / total    0.78537   0.65479   0.53922      6260

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 2, step: 441
training loss: 0.25822, validation loss: 0.26367
accuracy: 0.97396, macro average f1 score: 0.97263
classification report:
             precision    recall  f1-score   support

          0    0.98232   0.96689   0.97454      2930
          1    0.93466   0.99096   0.96199      1328
          2    0.98984   0.97303   0.98136      2002

avg / total    0.97461   0.97396   0.97406      6260

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 3, step: 661
training loss: 0.21420, validation loss: 0.30764
accuracy: 0.96198, macro average f1 score: 0.95965
classification report:
             precision    recall  f1-score   support

          0    0.99744   0.93106   0.96311      2930
          1    0.89639   0.99021   0.94097      1328
          2    0.96161   0.98851   0.97488      2002

avg / total    0.96455   0.96198   0.96217      6260

recording steps without improvement: 1
----------------------------------------------------
epoch: 4, step: 881
training loss: 0.19107, validation loss: 0.19366
accuracy: 0.97987, macro average f1 score: 0.97814
classification report:
             precision    recall  f1-score   support

          0    0.99613   0.96758   0.98165      2930
          1    0.93950   0.99398   0.96597      1328
          2    0.98507   0.98851   0.98679      2002

avg / total    0.98058   0.97987   0.97997      6260

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 5, step: 1101
training loss: 0.17617, validation loss: 0.22031
accuracy: 0.97524, macro average f1 score: 0.97328
classification report:
             precision    recall  f1-score   support

          0    0.99332   0.96382   0.97835      2930
          1    0.95004   0.97364   0.96170      1328
          2    0.96693   0.99301   0.97979      2002

avg / total    0.97570   0.97524   0.97528      6260

recording steps without improvement: 1
----------------------------------------------------
epoch: 6, step: 1321
training loss: 0.16561, validation loss: 0.19466
accuracy: 0.97955, macro average f1 score: 0.97727
classification report:
             precision    recall  f1-score   support

          0    0.99476   0.97099   0.98273      2930
          1    0.93471   0.99172   0.96237      1328
          2    0.98945   0.98402   0.98673      2002

avg / total    0.98032   0.97955   0.97969      6260

recording steps without improvement: 2
----------------------------------------------------
epoch: 7, step: 1541
training loss: 0.16308, validation loss: 0.17641
accuracy: 0.98099, macro average f1 score: 0.97932
classification report:
             precision    recall  f1-score   support

          0    0.99371   0.97031   0.98187      2930
          1    0.94337   0.99096   0.96658      1328
          2    0.98902   0.99001   0.98952      2002

avg / total    0.98153   0.98099   0.98107      6260

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 8, step: 1761
training loss: 0.15659, validation loss: 0.19346
accuracy: 0.98195, macro average f1 score: 0.98042
classification report:
             precision    recall  f1-score   support

          0    0.99031   0.97679   0.98351      2930
          1    0.94885   0.99172   0.96981      1328
          2    0.99294   0.98302   0.98795      2002

avg / total    0.98235   0.98195   0.98202      6260

recording steps without improvement: 1
----------------------------------------------------
epoch: 9, step: 1981
training loss: 0.15613, validation loss: 0.22570
accuracy: 0.97652, macro average f1 score: 0.97473
classification report:
             precision    recall  f1-score   support

          0    0.98409   0.97133   0.97767      2930
          1    0.95273   0.97139   0.96197      1328
          2    0.98163   0.98751   0.98456      2002

avg / total    0.97665   0.97652   0.97654      6260

recording steps without improvement: 2
----------------------------------------------------
epoch: 10, step: 2201
training loss: 0.15068, validation loss: 0.15840
accuracy: 0.98355, macro average f1 score: 0.98194
classification report:
             precision    recall  f1-score   support

          0    0.98965   0.97884   0.98421      2930
          1    0.95474   0.98494   0.96961      1328
          2    0.99448   0.98951   0.99199      2002

avg / total    0.98379   0.98355   0.98360      6260

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 11, step: 2421
training loss: 0.14269, validation loss: 0.17487
accuracy: 0.98291, macro average f1 score: 0.98106
classification report:
             precision    recall  f1-score   support

          0    0.99374   0.97509   0.98432      2930
          1    0.94604   0.99021   0.96762      1328
          2    0.99298   0.98951   0.99124      2002

avg / total    0.98338   0.98291   0.98299      6260

recording steps without improvement: 1
----------------------------------------------------
epoch: 12, step: 2641
training loss: 0.14000, validation loss: 0.17708
accuracy: 0.98307, macro average f1 score: 0.98137
classification report:
             precision    recall  f1-score   support

          0    0.99067   0.97816   0.98437      2930
          1    0.95007   0.98870   0.96900      1328
          2    0.99496   0.98651   0.99072      2002

avg / total    0.98343   0.98307   0.98314      6260

recording steps without improvement: 2
----------------------------------------------------
epoch: 13, step: 2861
training loss: 0.13284, validation loss: 0.18393
accuracy: 0.98243, macro average f1 score: 0.98078
classification report:
             precision    recall  f1-score   support

          0    0.98694   0.98020   0.98356      2930
          1    0.95601   0.98193   0.96880      1328
          2    0.99396   0.98601   0.98997      2002

avg / total    0.98262   0.98243   0.98248      6260

recording steps without improvement: 3
----------------------------------------------------
epoch: 14, step: 3081
training loss: 0.13301, validation loss: 0.19515
accuracy: 0.98179, macro average f1 score: 0.97999
classification report:
             precision    recall  f1-score   support

          0    0.99407   0.97304   0.98344      2930
          1    0.94536   0.99021   0.96727      1328
          2    0.98951   0.98901   0.98926      2002

avg / total    0.98228   0.98179   0.98187      6260

recording steps without improvement: 4
----------------------------------------------------
epoch: 15, step: 3301
training loss: 0.12715, validation loss: 0.25754
accuracy: 0.97492, macro average f1 score: 0.97160
classification report:
             precision    recall  f1-score   support

          0    0.99062   0.97372   0.98210      2930
          1    0.96113   0.94955   0.95530      1328
          2    0.96180   0.99351   0.97740      2002

avg / total    0.97515   0.97492   0.97491      6260

recording steps without improvement: 5
----------------------------------------------------
epoch: 16, step: 3521
training loss: 0.12898, validation loss: 0.16127
accuracy: 0.98323, macro average f1 score: 0.98156
classification report:
             precision    recall  f1-score   support

          0    0.99168   0.97679   0.98418      2930
          1    0.95405   0.98494   0.96925      1328
          2    0.99101   0.99151   0.99126      2002

avg / total    0.98349   0.98323   0.98328      6260

recording steps without improvement: 6
----------------------------------------------------
epoch: 17, step: 3741
training loss: 0.12497, validation loss: 0.15356
accuracy: 0.98482, macro average f1 score: 0.98336
classification report:
             precision    recall  f1-score   support

          0    0.99002   0.98157   0.98578      2930
          1    0.95766   0.98795   0.97257      1328
          2    0.99597   0.98751   0.99172      2002

avg / total    0.98506   0.98482   0.98488      6260

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 18, step: 3961
training loss: 0.12253, validation loss: 0.16754
accuracy: 0.98307, macro average f1 score: 0.98104
classification report:
             precision    recall  f1-score   support

          0    0.99375   0.97747   0.98555      2930
          1    0.94669   0.98946   0.96760      1328
          2    0.99296   0.98701   0.98998      2002

avg / total    0.98352   0.98307   0.98316      6260

recording steps without improvement: 1
----------------------------------------------------
epoch: 19, step: 4181
training loss: 0.12462, validation loss: 0.18506
accuracy: 0.97987, macro average f1 score: 0.97823
classification report:
             precision    recall  f1-score   support

          0    0.99231   0.96928   0.98066      2930
          1    0.94585   0.98645   0.96572      1328
          2    0.98559   0.99101   0.98829      2002

avg / total    0.98031   0.97987   0.97993      6260

recording steps without improvement: 2
----------------------------------------------------
epoch: 20, step: 4401
training loss: 0.12004, validation loss: 0.19078
accuracy: 0.98083, macro average f1 score: 0.97947
classification report:
             precision    recall  f1-score   support

          0    0.99371   0.97031   0.98187      2930
          1    0.95145   0.98870   0.96972      1328
          2    0.98266   0.99101   0.98682      2002

avg / total    0.98121   0.98083   0.98087      6260

recording steps without improvement: 3
----------------------------------------------------
epoch: 21, step: 4621
training loss: 0.12023, validation loss: 0.17436
accuracy: 0.98307, macro average f1 score: 0.98149
classification report:
             precision    recall  f1-score   support

          0    0.98530   0.98362   0.98446      2930
          1    0.96021   0.98117   0.97058      1328
          2    0.99545   0.98352   0.98945      2002

avg / total    0.98322   0.98307   0.98311      6260

recording steps without improvement: 4
----------------------------------------------------
epoch: 22, step: 4841
training loss: 0.11556, validation loss: 0.24438
accuracy: 0.97923, macro average f1 score: 0.97709
classification report:
             precision    recall  f1-score   support

          0    0.99269   0.97270   0.98259      2930
          1    0.93484   0.99398   0.96350      1328
          2    0.99140   0.97902   0.98517      2002

avg / total    0.98000   0.97923   0.97937      6260

recording steps without improvement: 5
----------------------------------------------------
epoch: 23, step: 5061
training loss: 0.11401, validation loss: 0.17710
accuracy: 0.98195, macro average f1 score: 0.98025
classification report:
             precision    recall  f1-score   support

          0    0.99098   0.97543   0.98314      2930
          1    0.96003   0.97666   0.96827      1328
          2    0.98370   0.99500   0.98932      2002

avg / total    0.98209   0.98195   0.98196      6260

recording steps without improvement: 6
----------------------------------------------------
epoch: 24, step: 5281
training loss: 0.11461, validation loss: 0.17711
accuracy: 0.98371, macro average f1 score: 0.98208
classification report:
             precision    recall  f1-score   support

          0    0.98798   0.98191   0.98494      2930
          1    0.96021   0.98117   0.97058      1328
          2    0.99347   0.98801   0.99073      2002

avg / total    0.98384   0.98371   0.98374      6260

recording steps without improvement: 7
----------------------------------------------------
epoch: 25, step: 5501
training loss: 0.10803, validation loss: 0.17954
accuracy: 0.98403, macro average f1 score: 0.98256
classification report:
             precision    recall  f1-score   support

          0    0.99272   0.97747   0.98504      2930
          1    0.95560   0.98870   0.97187      1328
          2    0.99100   0.99051   0.99076      2002

avg / total    0.98430   0.98403   0.98407      6260

recording steps without improvement: 8
----------------------------------------------------
epoch: 26, step: 5721
training loss: 0.11008, validation loss: 0.17057
accuracy: 0.98291, macro average f1 score: 0.98115
classification report:
             precision    recall  f1-score   support

          0    0.99035   0.98055   0.98542      2930
          1    0.95748   0.98343   0.97028      1328
          2    0.98947   0.98601   0.98774      2002

avg / total    0.98310   0.98291   0.98295      6260

recording steps without improvement: 9
----------------------------------------------------
epoch: 27, step: 5941
training loss: 0.10901, validation loss: 0.16161
accuracy: 0.98371, macro average f1 score: 0.98220
classification report:
             precision    recall  f1-score   support

          0    0.99512   0.97338   0.98413      2930
          1    0.94824   0.99322   0.97021      1328
          2    0.99201   0.99251   0.99226      2002

avg / total    0.98418   0.98371   0.98378      6260

recording steps without improvement: 10
----------------------------------------------------
test set evaluation
accuracy: 0.96281, macro average f1 score: 0.90118
classification report:
             precision    recall  f1-score   support

          0    0.96919   0.89598   0.93115      3932
          1    0.98494   0.97635   0.98062     17547
          2    0.65923   0.99105   0.79178       894

avg / total    0.96915   0.96281   0.96438     22373
confusion matrix:
[[ 3523   258   151]
 [  108 17132   307]
 [    4     4   886]]
