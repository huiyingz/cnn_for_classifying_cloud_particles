Arguments: Namespace(arch='VGG', augment=True, batch_size=128, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=50, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.0001, log_dir='logfiles', max_epochs=5000, model_dir='models/cnn_cloud_model_20180325-030321_VGG_res_BN_aug_merged_2016_JFJ_r3', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_JFJ_s1024_r3.pkl', predict=True, recording_steps=300, residual=True, resume_training=True, sigma=100.0, suffix='20180615-183133_VGG_res_BN_aug_merged_2016_JFJ_r3_TL_2016_JFJ_s1024_r3', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_JFJ_s1024_r3.pkl', train=True, train_set='prep_data/train_2016_JFJ_s1024_r3.pkl', use_class=None, use_shape=False, val=True, val_set='prep_data/val_2016_JFJ_s1024_r3.pkl', weigh_volume=False)

epoch: 1, step: 7
training loss: 0.60708, validation loss: 0.79375
accuracy: 0.95610, macro average f1 score: 0.92596
classification report:
             precision    recall  f1-score   support

          0    0.95425   0.99320   0.97333       147
          1    1.00000   0.84444   0.91566        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.95813   0.95610   0.95532       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 2, step: 13
training loss: 0.42543, validation loss: 0.69571
accuracy: 0.96585, macro average f1 score: 0.94567
classification report:
             precision    recall  f1-score   support

          0    0.96689   0.99320   0.97987       147
          1    1.00000   0.86667   0.92857        45
          2    0.86667   1.00000   0.92857        13

avg / total    0.96780   0.96585   0.96535       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 3, step: 19
training loss: 0.27071, validation loss: 0.67094
accuracy: 0.96585, macro average f1 score: 0.94567
classification report:
             precision    recall  f1-score   support

          0    0.96689   0.99320   0.97987       147
          1    1.00000   0.86667   0.92857        45
          2    0.86667   1.00000   0.92857        13

avg / total    0.96780   0.96585   0.96535       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 4, step: 25
training loss: 0.24387, validation loss: 0.65011
accuracy: 0.96585, macro average f1 score: 0.93664
classification report:
             precision    recall  f1-score   support

          0    0.96689   0.99320   0.97987       147
          1    1.00000   0.88889   0.94118        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.96720   0.96585   0.96560       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 5, step: 31
training loss: 0.20643, validation loss: 0.66082
accuracy: 0.96585, macro average f1 score: 0.93664
classification report:
             precision    recall  f1-score   support

          0    0.96689   0.99320   0.97987       147
          1    1.00000   0.88889   0.94118        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.96720   0.96585   0.96560       205

recording steps without improvement: 1
----------------------------------------------------
epoch: 6, step: 37
training loss: 0.16321, validation loss: 0.68521
accuracy: 0.96585, macro average f1 score: 0.93664
classification report:
             precision    recall  f1-score   support

          0    0.96689   0.99320   0.97987       147
          1    1.00000   0.88889   0.94118        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.96720   0.96585   0.96560       205

recording steps without improvement: 2
----------------------------------------------------
epoch: 7, step: 43
training loss: 0.14830, validation loss: 0.66792
accuracy: 0.96585, macro average f1 score: 0.93664
classification report:
             precision    recall  f1-score   support

          0    0.96689   0.99320   0.97987       147
          1    1.00000   0.88889   0.94118        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.96720   0.96585   0.96560       205

recording steps without improvement: 3
----------------------------------------------------
epoch: 8, step: 49
training loss: 0.15610, validation loss: 0.51993
accuracy: 0.96585, macro average f1 score: 0.93664
classification report:
             precision    recall  f1-score   support

          0    0.96689   0.99320   0.97987       147
          1    1.00000   0.88889   0.94118        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.96720   0.96585   0.96560       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 9, step: 55
training loss: 0.13900, validation loss: 0.41989
accuracy: 0.98049, macro average f1 score: 0.96228
classification report:
             precision    recall  f1-score   support

          0    0.97987   0.99320   0.98649       147
          1    1.00000   0.95556   0.97727        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.98068   0.98049   0.98044       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 10, step: 61
training loss: 0.10593, validation loss: 0.37471
accuracy: 0.98049, macro average f1 score: 0.96228
classification report:
             precision    recall  f1-score   support

          0    0.97987   0.99320   0.98649       147
          1    1.00000   0.95556   0.97727        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.98068   0.98049   0.98044       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 11, step: 67
training loss: 0.10140, validation loss: 0.34652
accuracy: 0.98537, macro average f1 score: 0.96722
classification report:
             precision    recall  f1-score   support

          0    0.98649   0.99320   0.98983       147
          1    1.00000   0.97778   0.98876        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.98543   0.98537   0.98536       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 12, step: 73
training loss: 0.09497, validation loss: 0.32868
accuracy: 0.98537, macro average f1 score: 0.96722
classification report:
             precision    recall  f1-score   support

          0    0.98649   0.99320   0.98983       147
          1    1.00000   0.97778   0.98876        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.98543   0.98537   0.98536       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 13, step: 79
training loss: 0.09287, validation loss: 0.33218
accuracy: 0.98537, macro average f1 score: 0.96722
classification report:
             precision    recall  f1-score   support

          0    0.98649   0.99320   0.98983       147
          1    1.00000   0.97778   0.98876        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.98543   0.98537   0.98536       205

recording steps without improvement: 1
----------------------------------------------------
epoch: 14, step: 85
training loss: 0.11491, validation loss: 0.33508
accuracy: 0.98049, macro average f1 score: 0.95200
classification report:
             precision    recall  f1-score   support

          0    0.98649   0.99320   0.98983       147
          1    1.00000   0.95556   0.97727        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98125   0.98049   0.98067       205

recording steps without improvement: 2
----------------------------------------------------
epoch: 15, step: 91
training loss: 0.10248, validation loss: 0.31850
accuracy: 0.98049, macro average f1 score: 0.95200
classification report:
             precision    recall  f1-score   support

          0    0.98649   0.99320   0.98983       147
          1    1.00000   0.95556   0.97727        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98125   0.98049   0.98067       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 16, step: 97
training loss: 0.09729, validation loss: 0.27609
accuracy: 0.98537, macro average f1 score: 0.96722
classification report:
             precision    recall  f1-score   support

          0    0.98649   0.99320   0.98983       147
          1    1.00000   0.97778   0.98876        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.98543   0.98537   0.98536       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 17, step: 103
training loss: 0.06205, validation loss: 0.25794
accuracy: 0.98537, macro average f1 score: 0.96722
classification report:
             precision    recall  f1-score   support

          0    0.98649   0.99320   0.98983       147
          1    1.00000   0.97778   0.98876        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.98543   0.98537   0.98536       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 18, step: 109
training loss: 0.07110, validation loss: 0.25167
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 19, step: 115
training loss: 0.07828, validation loss: 0.25050
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 20, step: 121
training loss: 0.07023, validation loss: 0.24528
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 21, step: 127
training loss: 0.06869, validation loss: 0.23986
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 22, step: 133
training loss: 0.06524, validation loss: 0.23734
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 23, step: 139
training loss: 0.07706, validation loss: 0.28070
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 1
----------------------------------------------------
epoch: 24, step: 145
training loss: 0.08246, validation loss: 0.21545
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 25, step: 151
training loss: 0.05017, validation loss: 0.18947
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 26, step: 157
training loss: 0.06813, validation loss: 0.17215
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 27, step: 163
training loss: 0.06437, validation loss: 0.18209
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 1
----------------------------------------------------
epoch: 28, step: 169
training loss: 0.04463, validation loss: 0.17718
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 2
----------------------------------------------------
epoch: 29, step: 175
training loss: 0.05375, validation loss: 0.18763
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 3
----------------------------------------------------
epoch: 30, step: 181
training loss: 0.04429, validation loss: 0.18749
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 4
----------------------------------------------------
epoch: 31, step: 187
training loss: 0.04868, validation loss: 0.19593
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 5
----------------------------------------------------
epoch: 32, step: 193
training loss: 0.04105, validation loss: 0.20292
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 6
----------------------------------------------------
epoch: 33, step: 199
training loss: 0.04282, validation loss: 0.21002
accuracy: 0.98537, macro average f1 score: 0.95680
classification report:
             precision    recall  f1-score   support

          0    1.00000   0.99320   0.99659       147
          1    1.00000   0.95556   0.97727        45
          2    0.81250   1.00000   0.89655        13

avg / total    0.98811   0.98537   0.98600       205

recording steps without improvement: 7
----------------------------------------------------
epoch: 34, step: 205
training loss: 0.04145, validation loss: 0.21002
accuracy: 0.98049, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.95556   0.97727        45
          2    0.80000   0.92308   0.85714        13

avg / total    0.98244   0.98049   0.98107       205

recording steps without improvement: 8
----------------------------------------------------
epoch: 35, step: 211
training loss: 0.06889, validation loss: 0.25095
accuracy: 0.98049, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.95556   0.97727        45
          2    0.80000   0.92308   0.85714        13

avg / total    0.98244   0.98049   0.98107       205

recording steps without improvement: 9
----------------------------------------------------
epoch: 36, step: 217
training loss: 0.06956, validation loss: 0.29450
accuracy: 0.98049, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.95556   0.97727        45
          2    0.80000   0.92308   0.85714        13

avg / total    0.98244   0.98049   0.98107       205

recording steps without improvement: 10
----------------------------------------------------
epoch: 37, step: 223
training loss: 0.03739, validation loss: 0.25164
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 11
----------------------------------------------------
epoch: 38, step: 229
training loss: 0.04885, validation loss: 0.23994
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 12
----------------------------------------------------
epoch: 39, step: 235
training loss: 0.03731, validation loss: 0.22578
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 13
----------------------------------------------------
epoch: 40, step: 241
training loss: 0.03544, validation loss: 0.22771
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 14
----------------------------------------------------
epoch: 41, step: 247
training loss: 0.04467, validation loss: 0.25371
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 15
----------------------------------------------------
epoch: 42, step: 253
training loss: 0.03945, validation loss: 0.28033
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 16
----------------------------------------------------
epoch: 43, step: 259
training loss: 0.04081, validation loss: 0.28525
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 17
----------------------------------------------------
epoch: 44, step: 265
training loss: 0.03502, validation loss: 0.27061
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 18
----------------------------------------------------
epoch: 45, step: 271
training loss: 0.03549, validation loss: 0.24644
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 19
----------------------------------------------------
epoch: 46, step: 277
training loss: 0.03870, validation loss: 0.23053
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 20
----------------------------------------------------
epoch: 47, step: 283
training loss: 0.02433, validation loss: 0.21698
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 21
----------------------------------------------------
epoch: 48, step: 289
training loss: 0.02997, validation loss: 0.21121
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 22
----------------------------------------------------
epoch: 49, step: 295
training loss: 0.03608, validation loss: 0.18721
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 23
----------------------------------------------------
epoch: 50, step: 301
training loss: 0.03552, validation loss: 0.19225
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 24
----------------------------------------------------
epoch: 51, step: 307
training loss: 0.02166, validation loss: 0.19741
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 25
----------------------------------------------------
epoch: 52, step: 313
training loss: 0.03340, validation loss: 0.21668
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 26
----------------------------------------------------
epoch: 53, step: 319
training loss: 0.02156, validation loss: 0.22756
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 27
----------------------------------------------------
epoch: 54, step: 325
training loss: 0.02392, validation loss: 0.23607
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 28
----------------------------------------------------
epoch: 55, step: 331
training loss: 0.02273, validation loss: 0.24704
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 29
----------------------------------------------------
epoch: 56, step: 337
training loss: 0.02663, validation loss: 0.25700
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 30
----------------------------------------------------
epoch: 57, step: 343
training loss: 0.02904, validation loss: 0.25920
accuracy: 0.99024, macro average f1 score: 0.97209
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   1.00000   1.00000        45
          2    0.92308   0.92308   0.92308        13

avg / total    0.99024   0.99024   0.99024       205

recording steps without improvement: 31
----------------------------------------------------
epoch: 58, step: 349
training loss: 0.01748, validation loss: 0.26945
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 32
----------------------------------------------------
epoch: 59, step: 355
training loss: 0.02957, validation loss: 0.27890
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 33
----------------------------------------------------
epoch: 60, step: 361
training loss: 0.02108, validation loss: 0.27102
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 34
----------------------------------------------------
epoch: 61, step: 367
training loss: 0.02013, validation loss: 0.27078
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 35
----------------------------------------------------
epoch: 62, step: 373
training loss: 0.02170, validation loss: 0.27220
accuracy: 0.98049, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.95556   0.97727        45
          2    0.80000   0.92308   0.85714        13

avg / total    0.98244   0.98049   0.98107       205

recording steps without improvement: 36
----------------------------------------------------
epoch: 63, step: 379
training loss: 0.01982, validation loss: 0.26840
accuracy: 0.98049, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.95556   0.97727        45
          2    0.80000   0.92308   0.85714        13

avg / total    0.98244   0.98049   0.98107       205

recording steps without improvement: 37
----------------------------------------------------
epoch: 64, step: 385
training loss: 0.01842, validation loss: 0.26531
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 38
----------------------------------------------------
epoch: 65, step: 391
training loss: 0.01790, validation loss: 0.25394
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 39
----------------------------------------------------
epoch: 66, step: 397
training loss: 0.01610, validation loss: 0.24377
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 40
----------------------------------------------------
epoch: 67, step: 403
training loss: 0.01183, validation loss: 0.23976
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 41
----------------------------------------------------
epoch: 68, step: 409
training loss: 0.01290, validation loss: 0.25090
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 42
----------------------------------------------------
epoch: 69, step: 415
training loss: 0.01613, validation loss: 0.23930
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 43
----------------------------------------------------
epoch: 70, step: 421
training loss: 0.01582, validation loss: 0.23453
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 44
----------------------------------------------------
epoch: 71, step: 427
training loss: 0.01208, validation loss: 0.23577
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 45
----------------------------------------------------
epoch: 72, step: 433
training loss: 0.01213, validation loss: 0.24577
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 46
----------------------------------------------------
epoch: 73, step: 439
training loss: 0.01004, validation loss: 0.25118
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 47
----------------------------------------------------
epoch: 74, step: 445
training loss: 0.01324, validation loss: 0.26035
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 48
----------------------------------------------------
epoch: 75, step: 451
training loss: 0.01266, validation loss: 0.27486
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 49
----------------------------------------------------
epoch: 76, step: 457
training loss: 0.01568, validation loss: 0.28352
accuracy: 0.98537, macro average f1 score: 0.95695
classification report:
             precision    recall  f1-score   support

          0    0.99320   0.99320   0.99320       147
          1    1.00000   0.97778   0.98876        45
          2    0.85714   0.92308   0.88889        13

avg / total    0.98606   0.98537   0.98561       205

recording steps without improvement: 50
----------------------------------------------------
test set evaluation
accuracy: 0.98580, macro average f1 score: 0.96931
classification report:
             precision    recall  f1-score   support

          0    0.99296   0.99320   0.99308      4262
          1    0.98645   0.97456   0.98047      1494
          2    0.91703   0.95238   0.93437       441

avg / total    0.98599   0.98580   0.98586      6197
confusion matrix:
[[4233    9   20]
 [  20 1456   18]
 [  10   11  420]]
