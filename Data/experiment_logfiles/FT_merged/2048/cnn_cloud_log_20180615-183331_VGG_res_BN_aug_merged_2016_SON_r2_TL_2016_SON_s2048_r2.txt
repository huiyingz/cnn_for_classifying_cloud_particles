Arguments: Namespace(arch='VGG', augment=True, batch_size=128, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=50, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.0001, log_dir='logfiles', max_epochs=5000, model_dir='models/cnn_cloud_model_20180325-030323_VGG_res_BN_aug_merged_2016_SON_r2', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_SON_s2048_r2.pkl', predict=True, recording_steps=300, residual=True, resume_training=True, sigma=100.0, suffix='20180615-183331_VGG_res_BN_aug_merged_2016_SON_r2_TL_2016_SON_s2048_r2', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_SON_s2048_r2.pkl', train=True, train_set='prep_data/train_2016_SON_s2048_r2.pkl', use_class=None, use_shape=False, val=True, val_set='prep_data/val_2016_SON_s2048_r2.pkl', weigh_volume=False)

epoch: 1, step: 13
training loss: 0.64085, validation loss: 0.93700
accuracy: 0.93171, macro average f1 score: 0.93168
classification report:
             precision    recall  f1-score   support

          0    0.95455   0.89634   0.92453       164
          1    0.91327   0.96237   0.93717       186
          2    0.93333   0.93333   0.93333        60

avg / total    0.93271   0.93171   0.93155       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 2, step: 25
training loss: 0.38036, validation loss: 0.74591
accuracy: 0.94634, macro average f1 score: 0.94854
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750       164
          1    0.92821   0.97312   0.95013       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94708   0.94634   0.94623       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 3, step: 37
training loss: 0.33867, validation loss: 0.70671
accuracy: 0.94634, macro average f1 score: 0.94854
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750       164
          1    0.92821   0.97312   0.95013       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94708   0.94634   0.94623       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 4, step: 49
training loss: 0.32221, validation loss: 0.64203
accuracy: 0.95366, macro average f1 score: 0.95797
classification report:
             precision    recall  f1-score   support

          0    0.96795   0.92073   0.94375       164
          1    0.93333   0.97849   0.95538       186
          2    0.98305   0.96667   0.97479        60

avg / total    0.95446   0.95366   0.95357       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 5, step: 61
training loss: 0.30566, validation loss: 0.64018
accuracy: 0.95366, macro average f1 score: 0.95815
classification report:
             precision    recall  f1-score   support

          0    0.96815   0.92683   0.94704       164
          1    0.93299   0.97312   0.95263       186
          2    0.98305   0.96667   0.97479        60

avg / total    0.95438   0.95366   0.95364       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 6, step: 73
training loss: 0.24588, validation loss: 0.67501
accuracy: 0.95610, macro average f1 score: 0.95827
classification report:
             precision    recall  f1-score   support

          0    0.96855   0.93902   0.95356       164
          1    0.93782   0.97312   0.95515       186
          2    0.98276   0.95000   0.96610        60

avg / total    0.95669   0.95610   0.95611       410

recording steps without improvement: 1
----------------------------------------------------
epoch: 7, step: 85
training loss: 0.27101, validation loss: 0.60187
accuracy: 0.95854, macro average f1 score: 0.96012
classification report:
             precision    recall  f1-score   support

          0    0.97452   0.93293   0.95327       164
          1    0.94301   0.97849   0.96042       186
          2    0.96667   0.96667   0.96667        60

avg / total    0.95907   0.95854   0.95848       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 8, step: 97
training loss: 0.26931, validation loss: 0.60280
accuracy: 0.95122, macro average f1 score: 0.95252
classification report:
             precision    recall  f1-score   support

          0    0.97419   0.92073   0.94671       164
          1    0.92857   0.97849   0.95288       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.95231   0.95122   0.95116       410

recording steps without improvement: 1
----------------------------------------------------
epoch: 9, step: 109
training loss: 0.25657, validation loss: 0.58510
accuracy: 0.94634, macro average f1 score: 0.94878
classification report:
             precision    recall  f1-score   support

          0    0.95031   0.93293   0.94154       164
          1    0.93684   0.95699   0.94681       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94651   0.94634   0.94634       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 10, step: 121
training loss: 0.23018, validation loss: 0.59869
accuracy: 0.94878, macro average f1 score: 0.95068
classification report:
             precision    recall  f1-score   support

          0    0.95625   0.93293   0.94444       164
          1    0.93717   0.96237   0.94960       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94904   0.94878   0.94877       410

recording steps without improvement: 1
----------------------------------------------------
epoch: 11, step: 133
training loss: 0.22234, validation loss: 0.58231
accuracy: 0.94634, macro average f1 score: 0.94872
classification report:
             precision    recall  f1-score   support

          0    0.96178   0.92073   0.94081       164
          1    0.92784   0.96774   0.94737       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94701   0.94634   0.94630       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 12, step: 145
training loss: 0.21492, validation loss: 0.62609
accuracy: 0.94634, macro average f1 score: 0.94878
classification report:
             precision    recall  f1-score   support

          0    0.95031   0.93293   0.94154       164
          1    0.93684   0.95699   0.94681       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94651   0.94634   0.94634       410

recording steps without improvement: 1
----------------------------------------------------
epoch: 13, step: 157
training loss: 0.21562, validation loss: 0.60862
accuracy: 0.94634, macro average f1 score: 0.94878
classification report:
             precision    recall  f1-score   support

          0    0.95031   0.93293   0.94154       164
          1    0.93684   0.95699   0.94681       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94651   0.94634   0.94634       410

recording steps without improvement: 2
----------------------------------------------------
epoch: 14, step: 169
training loss: 0.20381, validation loss: 0.55510
accuracy: 0.95122, macro average f1 score: 0.95244
classification report:
             precision    recall  f1-score   support

          0    0.95625   0.93293   0.94444       164
          1    0.94241   0.96774   0.95491       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.95141   0.95122   0.95117       410

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 15, step: 181
training loss: 0.19744, validation loss: 0.58226
accuracy: 0.94634, macro average f1 score: 0.94676
classification report:
             precision    recall  f1-score   support

          0    0.95031   0.93293   0.94154       164
          1    0.93717   0.96237   0.94960       186
          2    0.96552   0.93333   0.94915        60

avg / total    0.94658   0.94634   0.94631       410

recording steps without improvement: 1
----------------------------------------------------
epoch: 16, step: 193
training loss: 0.22128, validation loss: 0.57787
accuracy: 0.94878, macro average f1 score: 0.94863
classification report:
             precision    recall  f1-score   support

          0    0.96203   0.92683   0.94410       164
          1    0.93299   0.97312   0.95263       186
          2    0.96552   0.93333   0.94915        60

avg / total    0.94936   0.94878   0.94871       410

recording steps without improvement: 2
----------------------------------------------------
epoch: 17, step: 205
training loss: 0.20852, validation loss: 0.56392
accuracy: 0.94878, macro average f1 score: 0.95047
classification report:
             precision    recall  f1-score   support

          0    0.96178   0.92073   0.94081       164
          1    0.93299   0.97312   0.95263       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94935   0.94878   0.94869       410

recording steps without improvement: 3
----------------------------------------------------
epoch: 18, step: 217
training loss: 0.18632, validation loss: 0.58175
accuracy: 0.94634, macro average f1 score: 0.94671
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750       164
          1    0.93299   0.97312   0.95263       186
          2    0.95000   0.95000   0.95000        60

avg / total    0.94690   0.94634   0.94619       410

recording steps without improvement: 4
----------------------------------------------------
epoch: 19, step: 229
training loss: 0.18310, validation loss: 0.56712
accuracy: 0.95122, macro average f1 score: 0.95421
classification report:
             precision    recall  f1-score   support

          0    0.96178   0.92073   0.94081       164
          1    0.93782   0.97312   0.95515       186
          2    0.96667   0.96667   0.96667        60

avg / total    0.95163   0.95122   0.95110       410

recording steps without improvement: 5
----------------------------------------------------
epoch: 20, step: 241
training loss: 0.18346, validation loss: 0.57187
accuracy: 0.95122, macro average f1 score: 0.95421
classification report:
             precision    recall  f1-score   support

          0    0.96178   0.92073   0.94081       164
          1    0.93782   0.97312   0.95515       186
          2    0.96667   0.96667   0.96667        60

avg / total    0.95163   0.95122   0.95110       410

recording steps without improvement: 6
----------------------------------------------------
epoch: 21, step: 253
training loss: 0.17073, validation loss: 0.58361
accuracy: 0.94878, macro average f1 score: 0.95047
classification report:
             precision    recall  f1-score   support

          0    0.96178   0.92073   0.94081       164
          1    0.93299   0.97312   0.95263       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94935   0.94878   0.94869       410

recording steps without improvement: 7
----------------------------------------------------
epoch: 22, step: 265
training loss: 0.15246, validation loss: 0.59803
accuracy: 0.95122, macro average f1 score: 0.95228
classification report:
             precision    recall  f1-score   support

          0    0.95597   0.92683   0.94118       164
          1    0.94271   0.97312   0.95767       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.95144   0.95122   0.95112       410

recording steps without improvement: 8
----------------------------------------------------
epoch: 23, step: 277
training loss: 0.16819, validation loss: 0.58645
accuracy: 0.94878, macro average f1 score: 0.95039
classification report:
             precision    recall  f1-score   support

          0    0.95000   0.92683   0.93827       164
          1    0.94241   0.96774   0.95491       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94891   0.94878   0.94870       410

recording steps without improvement: 9
----------------------------------------------------
epoch: 24, step: 289
training loss: 0.17502, validation loss: 0.62410
accuracy: 0.94634, macro average f1 score: 0.94474
classification report:
             precision    recall  f1-score   support

          0    0.95570   0.92073   0.93789       164
          1    0.93782   0.97312   0.95515       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94663   0.94634   0.94620       410

recording steps without improvement: 10
----------------------------------------------------
epoch: 25, step: 301
training loss: 0.16560, validation loss: 0.60969
accuracy: 0.94878, macro average f1 score: 0.94853
classification report:
             precision    recall  f1-score   support

          0    0.95031   0.93293   0.94154       164
          1    0.94241   0.96774   0.95491       186
          2    0.96552   0.93333   0.94915        60

avg / total    0.94895   0.94878   0.94872       410

recording steps without improvement: 11
----------------------------------------------------
epoch: 26, step: 313
training loss: 0.17571, validation loss: 0.62649
accuracy: 0.94634, macro average f1 score: 0.94670
classification report:
             precision    recall  f1-score   support

          0    0.96178   0.92073   0.94081       164
          1    0.92821   0.97312   0.95013       186
          2    0.96552   0.93333   0.94915        60

avg / total    0.94710   0.94634   0.94626       410

recording steps without improvement: 12
----------------------------------------------------
epoch: 27, step: 325
training loss: 0.15553, validation loss: 0.63154
accuracy: 0.94634, macro average f1 score: 0.94495
classification report:
             precision    recall  f1-score   support

          0    0.95031   0.93293   0.94154       164
          1    0.94211   0.96237   0.95213       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94642   0.94634   0.94629       410

recording steps without improvement: 13
----------------------------------------------------
epoch: 28, step: 337
training loss: 0.14800, validation loss: 0.60213
accuracy: 0.94878, macro average f1 score: 0.95034
classification report:
             precision    recall  f1-score   support

          0    0.95570   0.92073   0.93789       164
          1    0.93782   0.97312   0.95515       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94911   0.94878   0.94866       410

recording steps without improvement: 14
----------------------------------------------------
epoch: 29, step: 349
training loss: 0.14900, validation loss: 0.61187
accuracy: 0.95122, macro average f1 score: 0.95228
classification report:
             precision    recall  f1-score   support

          0    0.95597   0.92683   0.94118       164
          1    0.94271   0.97312   0.95767       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.95144   0.95122   0.95112       410

recording steps without improvement: 15
----------------------------------------------------
epoch: 30, step: 361
training loss: 0.14668, validation loss: 0.61566
accuracy: 0.94878, macro average f1 score: 0.95039
classification report:
             precision    recall  f1-score   support

          0    0.95000   0.92683   0.93827       164
          1    0.94241   0.96774   0.95491       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94891   0.94878   0.94870       410

recording steps without improvement: 16
----------------------------------------------------
epoch: 31, step: 373
training loss: 0.12403, validation loss: 0.61907
accuracy: 0.94634, macro average f1 score: 0.94657
classification report:
             precision    recall  f1-score   support

          0    0.95541   0.91463   0.93458       164
          1    0.93782   0.97312   0.95515       186
          2    0.95000   0.95000   0.95000        60

avg / total    0.94664   0.94634   0.94617       410

recording steps without improvement: 17
----------------------------------------------------
epoch: 32, step: 385
training loss: 0.14383, validation loss: 0.58413
accuracy: 0.93902, macro average f1 score: 0.94284
classification report:
             precision    recall  f1-score   support

          0    0.92683   0.92683   0.92683       164
          1    0.94118   0.94624   0.94370       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.93909   0.93902   0.93904       410

recording steps without improvement: 18
----------------------------------------------------
epoch: 33, step: 397
training loss: 0.13493, validation loss: 0.61526
accuracy: 0.94390, macro average f1 score: 0.94661
classification report:
             precision    recall  f1-score   support

          0    0.93827   0.92683   0.93252       164
          1    0.94180   0.95699   0.94933       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94394   0.94390   0.94387       410

recording steps without improvement: 19
----------------------------------------------------
epoch: 34, step: 409
training loss: 0.13547, validation loss: 0.61209
accuracy: 0.94634, macro average f1 score: 0.94850
classification report:
             precision    recall  f1-score   support

          0    0.94410   0.92683   0.93538       164
          1    0.94211   0.96237   0.95213       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94641   0.94634   0.94629       410

recording steps without improvement: 20
----------------------------------------------------
epoch: 35, step: 421
training loss: 0.11299, validation loss: 0.62397
accuracy: 0.94146, macro average f1 score: 0.94274
classification report:
             precision    recall  f1-score   support

          0    0.94904   0.90854   0.92835       164
          1    0.93264   0.96774   0.94987       186
          2    0.95000   0.95000   0.95000        60

avg / total    0.94174   0.94146   0.94128       410

recording steps without improvement: 21
----------------------------------------------------
epoch: 36, step: 433
training loss: 0.11444, validation loss: 0.61381
accuracy: 0.94634, macro average f1 score: 0.94859
classification report:
             precision    recall  f1-score   support

          0    0.93333   0.93902   0.93617       164
          1    0.95161   0.95161   0.95161       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94642   0.94634   0.94637       410

recording steps without improvement: 22
----------------------------------------------------
epoch: 37, step: 445
training loss: 0.11997, validation loss: 0.61251
accuracy: 0.94390, macro average f1 score: 0.94651
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168       164
          1    0.93264   0.96774   0.94987       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94423   0.94390   0.94378       410

recording steps without improvement: 23
----------------------------------------------------
epoch: 38, step: 457
training loss: 0.09764, validation loss: 0.58521
accuracy: 0.94390, macro average f1 score: 0.94661
classification report:
             precision    recall  f1-score   support

          0    0.93827   0.92683   0.93252       164
          1    0.94180   0.95699   0.94933       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94394   0.94390   0.94387       410

recording steps without improvement: 24
----------------------------------------------------
epoch: 39, step: 469
training loss: 0.10122, validation loss: 0.61536
accuracy: 0.94390, macro average f1 score: 0.94651
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168       164
          1    0.93264   0.96774   0.94987       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94423   0.94390   0.94378       410

recording steps without improvement: 25
----------------------------------------------------
epoch: 40, step: 481
training loss: 0.12993, validation loss: 0.59681
accuracy: 0.94634, macro average f1 score: 0.94845
classification report:
             precision    recall  f1-score   support

          0    0.94969   0.92073   0.93498       164
          1    0.93750   0.96774   0.95238       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94656   0.94634   0.94624       410

recording steps without improvement: 26
----------------------------------------------------
epoch: 41, step: 493
training loss: 0.12051, validation loss: 0.62705
accuracy: 0.94634, macro average f1 score: 0.94657
classification report:
             precision    recall  f1-score   support

          0    0.95541   0.91463   0.93458       164
          1    0.93782   0.97312   0.95515       186
          2    0.95000   0.95000   0.95000        60

avg / total    0.94664   0.94634   0.94617       410

recording steps without improvement: 27
----------------------------------------------------
epoch: 42, step: 505
training loss: 0.10294, validation loss: 0.69870
accuracy: 0.94390, macro average f1 score: 0.94656
classification report:
             precision    recall  f1-score   support

          0    0.94375   0.92073   0.93210       164
          1    0.93717   0.96237   0.94960       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94404   0.94390   0.94383       410

recording steps without improvement: 28
----------------------------------------------------
epoch: 43, step: 517
training loss: 0.10615, validation loss: 0.61715
accuracy: 0.95366, macro average f1 score: 0.95421
classification report:
             precision    recall  f1-score   support

          0    0.95625   0.93293   0.94444       164
          1    0.94764   0.97312   0.96021       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.95379   0.95366   0.95358       410

recording steps without improvement: 29
----------------------------------------------------
epoch: 44, step: 529
training loss: 0.11373, validation loss: 0.66658
accuracy: 0.94390, macro average f1 score: 0.94651
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168       164
          1    0.93264   0.96774   0.94987       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94423   0.94390   0.94378       410

recording steps without improvement: 30
----------------------------------------------------
epoch: 45, step: 541
training loss: 0.09276, validation loss: 0.68058
accuracy: 0.93902, macro average f1 score: 0.94085
classification report:
             precision    recall  f1-score   support

          0    0.94304   0.90854   0.92547       164
          1    0.93229   0.96237   0.94709       186
          2    0.95000   0.95000   0.95000        60

avg / total    0.93918   0.93902   0.93887       410

recording steps without improvement: 31
----------------------------------------------------
epoch: 46, step: 553
training loss: 0.10369, validation loss: 0.63855
accuracy: 0.94878, macro average f1 score: 0.95039
classification report:
             precision    recall  f1-score   support

          0    0.95000   0.92683   0.93827       164
          1    0.94241   0.96774   0.95491       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94891   0.94878   0.94870       410

recording steps without improvement: 32
----------------------------------------------------
epoch: 47, step: 565
training loss: 0.08618, validation loss: 0.64810
accuracy: 0.94634, macro average f1 score: 0.94840
classification report:
             precision    recall  f1-score   support

          0    0.95541   0.91463   0.93458       164
          1    0.93299   0.97312   0.95263       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94681   0.94634   0.94619       410

recording steps without improvement: 33
----------------------------------------------------
epoch: 48, step: 577
training loss: 0.09581, validation loss: 0.61553
accuracy: 0.95366, macro average f1 score: 0.95430
classification report:
             precision    recall  f1-score   support

          0    0.94512   0.94512   0.94512       164
          1    0.95722   0.96237   0.95979       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.95368   0.95366   0.95366       410

recording steps without improvement: 34
----------------------------------------------------
epoch: 49, step: 589
training loss: 0.08304, validation loss: 0.64310
accuracy: 0.94878, macro average f1 score: 0.95039
classification report:
             precision    recall  f1-score   support

          0    0.95000   0.92683   0.93827       164
          1    0.94241   0.96774   0.95491       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.94891   0.94878   0.94870       410

recording steps without improvement: 35
----------------------------------------------------
epoch: 50, step: 601
training loss: 0.07904, validation loss: 0.66692
accuracy: 0.94634, macro average f1 score: 0.94492
classification report:
             precision    recall  f1-score   support

          0    0.94410   0.92683   0.93538       164
          1    0.95213   0.96237   0.95722       186
          2    0.93443   0.95000   0.94215        60

avg / total    0.94633   0.94634   0.94628       410

recording steps without improvement: 36
----------------------------------------------------
epoch: 51, step: 613
training loss: 0.08747, validation loss: 0.71352
accuracy: 0.94146, macro average f1 score: 0.94280
classification report:
             precision    recall  f1-score   support

          0    0.94340   0.91463   0.92879       164
          1    0.93717   0.96237   0.94960       186
          2    0.95000   0.95000   0.95000        60

avg / total    0.94154   0.94146   0.94134       410

recording steps without improvement: 37
----------------------------------------------------
epoch: 52, step: 625
training loss: 0.08730, validation loss: 0.73173
accuracy: 0.94390, macro average f1 score: 0.94285
classification report:
             precision    recall  f1-score   support

          0    0.94969   0.92073   0.93498       164
          1    0.93750   0.96774   0.95238       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94408   0.94390   0.94378       410

recording steps without improvement: 38
----------------------------------------------------
epoch: 53, step: 637
training loss: 0.07867, validation loss: 0.71768
accuracy: 0.94146, macro average f1 score: 0.94096
classification report:
             precision    recall  f1-score   support

          0    0.94375   0.92073   0.93210       164
          1    0.93717   0.96237   0.94960       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94156   0.94146   0.94137       410

recording steps without improvement: 39
----------------------------------------------------
epoch: 54, step: 649
training loss: 0.08472, validation loss: 0.73411
accuracy: 0.94878, macro average f1 score: 0.94852
classification report:
             precision    recall  f1-score   support

          0    0.95570   0.92073   0.93789       164
          1    0.94271   0.97312   0.95767       186
          2    0.95000   0.95000   0.95000        60

avg / total    0.94897   0.94878   0.94864       410

recording steps without improvement: 40
----------------------------------------------------
epoch: 55, step: 661
training loss: 0.07449, validation loss: 0.70873
accuracy: 0.94634, macro average f1 score: 0.94479
classification report:
             precision    recall  f1-score   support

          0    0.95000   0.92683   0.93827       164
          1    0.94241   0.96774   0.95491       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94643   0.94634   0.94624       410

recording steps without improvement: 41
----------------------------------------------------
epoch: 56, step: 673
training loss: 0.06426, validation loss: 0.72245
accuracy: 0.94146, macro average f1 score: 0.94096
classification report:
             precision    recall  f1-score   support

          0    0.94375   0.92073   0.93210       164
          1    0.93717   0.96237   0.94960       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94156   0.94146   0.94137       410

recording steps without improvement: 42
----------------------------------------------------
epoch: 57, step: 685
training loss: 0.06438, validation loss: 0.72147
accuracy: 0.94390, macro average f1 score: 0.94280
classification report:
             precision    recall  f1-score   support

          0    0.95541   0.91463   0.93458       164
          1    0.93299   0.97312   0.95263       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94432   0.94390   0.94373       410

recording steps without improvement: 43
----------------------------------------------------
epoch: 58, step: 697
training loss: 0.08392, validation loss: 0.74841
accuracy: 0.94390, macro average f1 score: 0.94285
classification report:
             precision    recall  f1-score   support

          0    0.94969   0.92073   0.93498       164
          1    0.93750   0.96774   0.95238       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94408   0.94390   0.94378       410

recording steps without improvement: 44
----------------------------------------------------
epoch: 59, step: 709
training loss: 0.07988, validation loss: 0.76594
accuracy: 0.94390, macro average f1 score: 0.94285
classification report:
             precision    recall  f1-score   support

          0    0.94969   0.92073   0.93498       164
          1    0.93750   0.96774   0.95238       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94408   0.94390   0.94378       410

recording steps without improvement: 45
----------------------------------------------------
epoch: 60, step: 721
training loss: 0.06186, validation loss: 0.75984
accuracy: 0.95366, macro average f1 score: 0.95227
classification report:
             precision    recall  f1-score   support

          0    0.96815   0.92683   0.94704       164
          1    0.93846   0.98387   0.96063       186
          2    0.96552   0.93333   0.94915        60

avg / total    0.95430   0.95366   0.95351       410

recording steps without improvement: 46
----------------------------------------------------
epoch: 61, step: 733
training loss: 0.07460, validation loss: 0.79680
accuracy: 0.94390, macro average f1 score: 0.94285
classification report:
             precision    recall  f1-score   support

          0    0.94969   0.92073   0.93498       164
          1    0.93750   0.96774   0.95238       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94408   0.94390   0.94378       410

recording steps without improvement: 47
----------------------------------------------------
epoch: 62, step: 745
training loss: 0.06231, validation loss: 0.85370
accuracy: 0.94146, macro average f1 score: 0.94091
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168       164
          1    0.93264   0.96774   0.94987       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94175   0.94146   0.94132       410

recording steps without improvement: 48
----------------------------------------------------
epoch: 63, step: 757
training loss: 0.05236, validation loss: 0.78533
accuracy: 0.94390, macro average f1 score: 0.94285
classification report:
             precision    recall  f1-score   support

          0    0.94969   0.92073   0.93498       164
          1    0.93750   0.96774   0.95238       186
          2    0.94915   0.93333   0.94118        60

avg / total    0.94408   0.94390   0.94378       410

recording steps without improvement: 49
----------------------------------------------------
epoch: 64, step: 769
training loss: 0.04814, validation loss: 0.79003
accuracy: 0.95366, macro average f1 score: 0.95412
classification report:
             precision    recall  f1-score   support

          0    0.96795   0.92073   0.94375       164
          1    0.93846   0.98387   0.96063       186
          2    0.96610   0.95000   0.95798        60

avg / total    0.95430   0.95366   0.95349       410

recording steps without improvement: 50
----------------------------------------------------
test set evaluation
accuracy: 0.96221, macro average f1 score: 0.96284
classification report:
             precision    recall  f1-score   support

          0    0.97140   0.94507   0.95805      5534
          1    0.95408   0.97571   0.96477      6133
          2    0.96296   0.96844   0.96570      1933

avg / total    0.96239   0.96221   0.96217     13600
confusion matrix:
[[5230  259   45]
 [ 122 5984   27]
 [  32   29 1872]]
