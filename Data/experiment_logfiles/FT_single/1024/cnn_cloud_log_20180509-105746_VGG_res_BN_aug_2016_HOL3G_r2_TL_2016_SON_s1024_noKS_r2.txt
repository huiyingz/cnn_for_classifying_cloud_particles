Arguments: Namespace(arch='VGG', augment=True, batch_size=128, batchnorm=True, domain_adaptation=False, domain_match=None, dropout_fc=1.0, dropout_in=1.0, early_stopping_steps=50, img_size=32, l2_reg=0, lamb=1.0, learning_rate=0.0001, log_dir='logfiles', max_epochs=5000, model_dir='models/cnn_cloud_model_20180324-185043_VGG_res_BN_aug_2016_HOL3G_r2', no_phase=False, pred_dir='predictions', pred_set='prep_data/test_2016_SON_s1024_noKS_r2.pkl', predict=True, recording_steps=300, residual=True, resume_training=True, sigma=100.0, suffix='20180509-105746_VGG_res_BN_aug_2016_HOL3G_r2_TL_2016_SON_s1024_noKS_r2', tSNE=False, tSNE_dir='tSNE', targ_set='prep_data/test_classified_32.pkl', test=True, test_set='prep_data/test_2016_SON_s1024_noKS_r2.pkl', train=True, train_set='prep_data/train_2016_SON_s1024_noKS_r2.pkl', use_class=None, use_shape=False, val=True, val_set='prep_data/val_2016_SON_s1024_noKS_r2.pkl', weigh_volume=False)

epoch: 1, step: 7
training loss: 2.26650, validation loss: 2.25103
accuracy: 0.92195, macro average f1 score: 0.90736
classification report:
             precision    recall  f1-score   support

          0    0.96203   0.92683   0.94410        82
          1    0.94565   0.90625   0.92553        96
          2    0.76471   0.96296   0.85246        27

avg / total    0.92837   0.92195   0.92333       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 2, step: 13
training loss: 1.15443, validation loss: 2.22607
accuracy: 0.91707, macro average f1 score: 0.90057
classification report:
             precision    recall  f1-score   support

          0    0.95000   0.92683   0.93827        82
          1    0.95556   0.89583   0.92473        96
          2    0.74286   0.96296   0.83871        27

avg / total    0.92532   0.91707   0.91882       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 3, step: 19
training loss: 0.96232, validation loss: 2.13854
accuracy: 0.91220, macro average f1 score: 0.89641
classification report:
             precision    recall  f1-score   support

          0    0.94872   0.90244   0.92500        82
          1    0.94565   0.90625   0.92553        96
          2    0.74286   0.96296   0.83871        27

avg / total    0.92017   0.91220   0.91388       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 4, step: 25
training loss: 0.86737, validation loss: 1.90866
accuracy: 0.91220, macro average f1 score: 0.89636
classification report:
             precision    recall  f1-score   support

          0    0.96053   0.89024   0.92405        82
          1    0.93617   0.91667   0.92632        96
          2    0.74286   0.96296   0.83871        27

avg / total    0.92045   0.91220   0.91387       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 5, step: 31
training loss: 0.72735, validation loss: 1.75933
accuracy: 0.90732, macro average f1 score: 0.88808
classification report:
             precision    recall  f1-score   support

          0    0.94805   0.89024   0.91824        82
          1    0.93617   0.91667   0.92632        96
          2    0.73529   0.92593   0.81967        27

avg / total    0.91447   0.90732   0.90904       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 6, step: 37
training loss: 0.50617, validation loss: 1.74099
accuracy: 0.91220, macro average f1 score: 0.89900
classification report:
             precision    recall  f1-score   support

          0    0.94805   0.89024   0.91824        82
          1    0.93617   0.91667   0.92632        96
          2    0.76471   0.96296   0.85246        27

avg / total    0.91834   0.91220   0.91336       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 7, step: 43
training loss: 0.46625, validation loss: 1.80608
accuracy: 0.90732, macro average f1 score: 0.89520
classification report:
             precision    recall  f1-score   support

          0    0.93590   0.89024   0.91250        82
          1    0.93548   0.90625   0.92063        96
          2    0.76471   0.96296   0.85246        27

avg / total    0.91316   0.90732   0.90840       205

recording steps without improvement: 1
----------------------------------------------------
epoch: 8, step: 49
training loss: 0.42644, validation loss: 1.83959
accuracy: 0.90244, macro average f1 score: 0.89139
classification report:
             precision    recall  f1-score   support

          0    0.92405   0.89024   0.90683        82
          1    0.93478   0.89583   0.91489        96
          2    0.76471   0.96296   0.85246        27

avg / total    0.90809   0.90244   0.90345       205

recording steps without improvement: 2
----------------------------------------------------
epoch: 9, step: 55
training loss: 0.45291, validation loss: 1.83030
accuracy: 0.91220, macro average f1 score: 0.89900
classification report:
             precision    recall  f1-score   support

          0    0.94805   0.89024   0.91824        82
          1    0.93617   0.91667   0.92632        96
          2    0.76471   0.96296   0.85246        27

avg / total    0.91834   0.91220   0.91336       205

recording steps without improvement: 3
----------------------------------------------------
epoch: 10, step: 61
training loss: 0.43382, validation loss: 1.80183
accuracy: 0.92195, macro average f1 score: 0.90981
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.93684   0.92708   0.93194        96
          2    0.78788   0.96296   0.86667        27

avg / total    0.92690   0.92195   0.92289       205

recording steps without improvement: 4
----------------------------------------------------
epoch: 11, step: 67
training loss: 0.37067, validation loss: 1.81033
accuracy: 0.92195, macro average f1 score: 0.90941
classification report:
             precision    recall  f1-score   support

          0    0.96053   0.89024   0.92405        82
          1    0.93750   0.93750   0.93750        96
          2    0.78788   0.96296   0.86667        27

avg / total    0.92700   0.92195   0.92279       205

recording steps without improvement: 5
----------------------------------------------------
epoch: 12, step: 73
training loss: 0.36168, validation loss: 1.82806
accuracy: 0.91707, macro average f1 score: 0.90561
classification report:
             precision    recall  f1-score   support

          0    0.94805   0.89024   0.91824        82
          1    0.93684   0.92708   0.93194        96
          2    0.78788   0.96296   0.86667        27

avg / total    0.92171   0.91707   0.91786       205

recording steps without improvement: 6
----------------------------------------------------
epoch: 13, step: 79
training loss: 0.39051, validation loss: 1.81675
accuracy: 0.91220, macro average f1 score: 0.90183
classification report:
             precision    recall  f1-score   support

          0    0.93590   0.89024   0.91250        82
          1    0.93617   0.91667   0.92632        96
          2    0.78788   0.96296   0.86667        27

avg / total    0.91653   0.91220   0.91293       205

recording steps without improvement: 7
----------------------------------------------------
epoch: 14, step: 85
training loss: 0.32979, validation loss: 1.77813
accuracy: 0.91220, macro average f1 score: 0.90183
classification report:
             precision    recall  f1-score   support

          0    0.93590   0.89024   0.91250        82
          1    0.93617   0.91667   0.92632        96
          2    0.78788   0.96296   0.86667        27

avg / total    0.91653   0.91220   0.91293       205

recording steps without improvement: 8
----------------------------------------------------
epoch: 15, step: 91
training loss: 0.30912, validation loss: 1.71617
accuracy: 0.94146, macro average f1 score: 0.93856
classification report:
             precision    recall  f1-score   support

          0    0.96203   0.92683   0.94410        82
          1    0.93814   0.94792   0.94301        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94222   0.94146   0.94154       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 16, step: 97
training loss: 0.31925, validation loss: 1.66804
accuracy: 0.94146, macro average f1 score: 0.93856
classification report:
             precision    recall  f1-score   support

          0    0.96203   0.92683   0.94410        82
          1    0.93814   0.94792   0.94301        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94222   0.94146   0.94154       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 17, step: 103
training loss: 0.29933, validation loss: 1.65161
accuracy: 0.94146, macro average f1 score: 0.93856
classification report:
             precision    recall  f1-score   support

          0    0.96203   0.92683   0.94410        82
          1    0.93814   0.94792   0.94301        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94222   0.94146   0.94154       205

recording steps without improvement: 0, model saved
----------------------------------------------------
epoch: 18, step: 109
training loss: 0.27898, validation loss: 1.67169
accuracy: 0.94146, macro average f1 score: 0.93856
classification report:
             precision    recall  f1-score   support

          0    0.96203   0.92683   0.94410        82
          1    0.93814   0.94792   0.94301        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94222   0.94146   0.94154       205

recording steps without improvement: 1
----------------------------------------------------
epoch: 19, step: 115
training loss: 0.27983, validation loss: 1.71243
accuracy: 0.94146, macro average f1 score: 0.93856
classification report:
             precision    recall  f1-score   support

          0    0.96203   0.92683   0.94410        82
          1    0.93814   0.94792   0.94301        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94222   0.94146   0.94154       205

recording steps without improvement: 2
----------------------------------------------------
epoch: 20, step: 121
training loss: 0.22606, validation loss: 1.74410
accuracy: 0.94146, macro average f1 score: 0.93852
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92929   0.95833   0.94359        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94287   0.94146   0.94153       205

recording steps without improvement: 3
----------------------------------------------------
epoch: 21, step: 127
training loss: 0.22615, validation loss: 1.73605
accuracy: 0.94146, macro average f1 score: 0.93852
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92929   0.95833   0.94359        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94287   0.94146   0.94153       205

recording steps without improvement: 4
----------------------------------------------------
epoch: 22, step: 133
training loss: 0.26256, validation loss: 1.74954
accuracy: 0.93659, macro average f1 score: 0.93474
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750        82
          1    0.92857   0.94792   0.93814        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93754   0.93659   0.93663       205

recording steps without improvement: 5
----------------------------------------------------
epoch: 23, step: 139
training loss: 0.24633, validation loss: 1.76322
accuracy: 0.93659, macro average f1 score: 0.93474
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750        82
          1    0.92857   0.94792   0.93814        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93754   0.93659   0.93663       205

recording steps without improvement: 6
----------------------------------------------------
epoch: 24, step: 145
training loss: 0.23085, validation loss: 1.80518
accuracy: 0.93659, macro average f1 score: 0.93474
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750        82
          1    0.92857   0.94792   0.93814        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93754   0.93659   0.93663       205

recording steps without improvement: 7
----------------------------------------------------
epoch: 25, step: 151
training loss: 0.22270, validation loss: 1.84606
accuracy: 0.92683, macro average f1 score: 0.92287
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.91837   0.93750   0.92784        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92741   0.92683   0.92690       205

recording steps without improvement: 8
----------------------------------------------------
epoch: 26, step: 157
training loss: 0.21717, validation loss: 1.86702
accuracy: 0.92683, macro average f1 score: 0.92287
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.91837   0.93750   0.92784        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92741   0.92683   0.92690       205

recording steps without improvement: 9
----------------------------------------------------
epoch: 27, step: 163
training loss: 0.20098, validation loss: 1.86861
accuracy: 0.93171, macro average f1 score: 0.92664
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750        82
          1    0.91919   0.94792   0.93333        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.93266   0.93171   0.93181       205

recording steps without improvement: 10
----------------------------------------------------
epoch: 28, step: 169
training loss: 0.19208, validation loss: 1.86893
accuracy: 0.94146, macro average f1 score: 0.93421
classification report:
             precision    recall  f1-score   support

          0    0.98684   0.91463   0.94937        82
          1    0.92079   0.96875   0.94416        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.94353   0.94146   0.94163       205

recording steps without improvement: 11
----------------------------------------------------
epoch: 29, step: 175
training loss: 0.22640, validation loss: 1.86669
accuracy: 0.93659, macro average f1 score: 0.93042
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92000   0.95833   0.93878        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.93804   0.93659   0.93671       205

recording steps without improvement: 12
----------------------------------------------------
epoch: 30, step: 181
training loss: 0.17470, validation loss: 1.86967
accuracy: 0.93171, macro average f1 score: 0.92664
classification report:
             precision    recall  f1-score   support

          0    0.96154   0.91463   0.93750        82
          1    0.91919   0.94792   0.93333        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.93266   0.93171   0.93181       205

recording steps without improvement: 13
----------------------------------------------------
epoch: 31, step: 187
training loss: 0.17863, validation loss: 1.86265
accuracy: 0.93659, macro average f1 score: 0.93042
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92000   0.95833   0.93878        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.93804   0.93659   0.93671       205

recording steps without improvement: 14
----------------------------------------------------
epoch: 32, step: 193
training loss: 0.18936, validation loss: 1.80248
accuracy: 0.94634, macro average f1 score: 0.94635
classification report:
             precision    recall  f1-score   support

          0    0.97436   0.92683   0.95000        82
          1    0.92929   0.95833   0.94359        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.94722   0.94634   0.94640       205

recording steps without improvement: 15
----------------------------------------------------
epoch: 33, step: 199
training loss: 0.18904, validation loss: 1.80211
accuracy: 0.94634, macro average f1 score: 0.94635
classification report:
             precision    recall  f1-score   support

          0    0.97436   0.92683   0.95000        82
          1    0.92929   0.95833   0.94359        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.94722   0.94634   0.94640       205

recording steps without improvement: 16
----------------------------------------------------
epoch: 34, step: 205
training loss: 0.16540, validation loss: 1.84840
accuracy: 0.95122, macro average f1 score: 0.95014
classification report:
             precision    recall  f1-score   support

          0    0.98701   0.92683   0.95597        82
          1    0.93000   0.96875   0.94898        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.95262   0.95122   0.95131       205

recording steps without improvement: 17
----------------------------------------------------
epoch: 35, step: 211
training loss: 0.17072, validation loss: 1.86104
accuracy: 0.94146, macro average f1 score: 0.93852
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92929   0.95833   0.94359        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.94287   0.94146   0.94153       205

recording steps without improvement: 18
----------------------------------------------------
epoch: 36, step: 217
training loss: 0.16707, validation loss: 1.81336
accuracy: 0.92195, macro average f1 score: 0.92343
classification report:
             precision    recall  f1-score   support

          0    0.92593   0.91463   0.92025        82
          1    0.92632   0.91667   0.92147        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.92224   0.92195   0.92191       205

recording steps without improvement: 19
----------------------------------------------------
epoch: 37, step: 223
training loss: 0.16266, validation loss: 1.82477
accuracy: 0.94634, macro average f1 score: 0.94635
classification report:
             precision    recall  f1-score   support

          0    0.97436   0.92683   0.95000        82
          1    0.92929   0.95833   0.94359        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.94722   0.94634   0.94640       205

recording steps without improvement: 20
----------------------------------------------------
epoch: 38, step: 229
training loss: 0.15542, validation loss: 1.89055
accuracy: 0.94146, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92000   0.95833   0.93878        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.94274   0.94146   0.94150       205

recording steps without improvement: 21
----------------------------------------------------
epoch: 39, step: 235
training loss: 0.12419, validation loss: 1.95631
accuracy: 0.93659, macro average f1 score: 0.93444
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.91089   0.95833   0.93401        96
          2    0.92593   0.92593   0.92593        27

avg / total    0.93813   0.93659   0.93670       205

recording steps without improvement: 22
----------------------------------------------------
epoch: 40, step: 241
training loss: 0.11788, validation loss: 1.96537
accuracy: 0.93659, macro average f1 score: 0.93444
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.91089   0.95833   0.93401        96
          2    0.92593   0.92593   0.92593        27

avg / total    0.93813   0.93659   0.93670       205

recording steps without improvement: 23
----------------------------------------------------
epoch: 41, step: 247
training loss: 0.12664, validation loss: 1.97579
accuracy: 0.94146, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92000   0.95833   0.93878        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.94274   0.94146   0.94150       205

recording steps without improvement: 24
----------------------------------------------------
epoch: 42, step: 253
training loss: 0.12894, validation loss: 1.98216
accuracy: 0.94146, macro average f1 score: 0.94254
classification report:
             precision    recall  f1-score   support

          0    0.97403   0.91463   0.94340        82
          1    0.92000   0.95833   0.93878        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.94274   0.94146   0.94150       205

recording steps without improvement: 25
----------------------------------------------------
epoch: 43, step: 259
training loss: 0.13965, validation loss: 1.98390
accuracy: 0.93659, macro average f1 score: 0.93469
classification report:
             precision    recall  f1-score   support

          0    0.97368   0.90244   0.93671        82
          1    0.92000   0.95833   0.93878        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93839   0.93659   0.93660       205

recording steps without improvement: 26
----------------------------------------------------
epoch: 44, step: 265
training loss: 0.12614, validation loss: 1.99508
accuracy: 0.92683, macro average f1 score: 0.92287
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.91837   0.93750   0.92784        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92741   0.92683   0.92690       205

recording steps without improvement: 27
----------------------------------------------------
epoch: 45, step: 271
training loss: 0.13295, validation loss: 1.96323
accuracy: 0.93171, macro average f1 score: 0.93096
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.92784   0.93750   0.93264        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93233   0.93171   0.93172       205

recording steps without improvement: 28
----------------------------------------------------
epoch: 46, step: 277
training loss: 0.13906, validation loss: 1.95272
accuracy: 0.93171, macro average f1 score: 0.93096
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.92784   0.93750   0.93264        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93233   0.93171   0.93172       205

recording steps without improvement: 29
----------------------------------------------------
epoch: 47, step: 283
training loss: 0.11485, validation loss: 1.97013
accuracy: 0.93171, macro average f1 score: 0.93091
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91919   0.94792   0.93333        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93295   0.93171   0.93170       205

recording steps without improvement: 30
----------------------------------------------------
epoch: 48, step: 289
training loss: 0.14258, validation loss: 2.07607
accuracy: 0.92195, macro average f1 score: 0.91516
classification report:
             precision    recall  f1-score   support

          0    0.96053   0.89024   0.92405        82
          1    0.91000   0.94792   0.92857        96
          2    0.86207   0.92593   0.89286        27

avg / total    0.92390   0.92195   0.92206       205

recording steps without improvement: 31
----------------------------------------------------
epoch: 49, step: 295
training loss: 0.09715, validation loss: 2.12461
accuracy: 0.92195, macro average f1 score: 0.91516
classification report:
             precision    recall  f1-score   support

          0    0.96053   0.89024   0.92405        82
          1    0.91000   0.94792   0.92857        96
          2    0.86207   0.92593   0.89286        27

avg / total    0.92390   0.92195   0.92206       205

recording steps without improvement: 32
----------------------------------------------------
epoch: 50, step: 301
training loss: 0.12659, validation loss: 2.13490
accuracy: 0.92683, macro average f1 score: 0.92283
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91000   0.94792   0.92857        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92816   0.92683   0.92690       205

recording steps without improvement: 33
----------------------------------------------------
epoch: 51, step: 307
training loss: 0.12151, validation loss: 2.03837
accuracy: 0.93171, macro average f1 score: 0.93091
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91919   0.94792   0.93333        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93295   0.93171   0.93170       205

recording steps without improvement: 34
----------------------------------------------------
epoch: 52, step: 313
training loss: 0.09721, validation loss: 1.95621
accuracy: 0.93659, macro average f1 score: 0.93879
classification report:
             precision    recall  f1-score   support

          0    0.95000   0.92683   0.93827        82
          1    0.92784   0.93750   0.93264        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.93680   0.93659   0.93658       205

recording steps without improvement: 35
----------------------------------------------------
epoch: 53, step: 319
training loss: 0.08998, validation loss: 1.98426
accuracy: 0.93171, macro average f1 score: 0.93096
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.92784   0.93750   0.93264        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93233   0.93171   0.93172       205

recording steps without improvement: 36
----------------------------------------------------
epoch: 54, step: 325
training loss: 0.08370, validation loss: 2.05544
accuracy: 0.93171, macro average f1 score: 0.93091
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91919   0.94792   0.93333        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93295   0.93171   0.93170       205

recording steps without improvement: 37
----------------------------------------------------
epoch: 55, step: 331
training loss: 0.09073, validation loss: 2.07019
accuracy: 0.93171, macro average f1 score: 0.93091
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91919   0.94792   0.93333        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93295   0.93171   0.93170       205

recording steps without improvement: 38
----------------------------------------------------
epoch: 56, step: 337
training loss: 0.08086, validation loss: 2.08013
accuracy: 0.93171, macro average f1 score: 0.93091
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91919   0.94792   0.93333        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.93295   0.93171   0.93170       205

recording steps without improvement: 39
----------------------------------------------------
epoch: 57, step: 343
training loss: 0.08474, validation loss: 2.11704
accuracy: 0.92195, macro average f1 score: 0.91906
classification report:
             precision    recall  f1-score   support

          0    0.94872   0.90244   0.92500        82
          1    0.90909   0.93750   0.92308        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92280   0.92195   0.92200       205

recording steps without improvement: 40
----------------------------------------------------
epoch: 58, step: 349
training loss: 0.12185, validation loss: 2.15394
accuracy: 0.92195, macro average f1 score: 0.91906
classification report:
             precision    recall  f1-score   support

          0    0.94872   0.90244   0.92500        82
          1    0.90909   0.93750   0.92308        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92280   0.92195   0.92200       205

recording steps without improvement: 41
----------------------------------------------------
epoch: 59, step: 355
training loss: 0.11499, validation loss: 2.23039
accuracy: 0.92683, macro average f1 score: 0.92283
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91000   0.94792   0.92857        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92816   0.92683   0.92690       205

recording steps without improvement: 42
----------------------------------------------------
epoch: 60, step: 361
training loss: 0.10252, validation loss: 2.19604
accuracy: 0.92683, macro average f1 score: 0.92283
classification report:
             precision    recall  f1-score   support

          0    0.96104   0.90244   0.93082        82
          1    0.91000   0.94792   0.92857        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92816   0.92683   0.92690       205

recording steps without improvement: 43
----------------------------------------------------
epoch: 61, step: 367
training loss: 0.06567, validation loss: 2.14136
accuracy: 0.92195, macro average f1 score: 0.91906
classification report:
             precision    recall  f1-score   support

          0    0.94872   0.90244   0.92500        82
          1    0.90909   0.93750   0.92308        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92280   0.92195   0.92200       205

recording steps without improvement: 44
----------------------------------------------------
epoch: 62, step: 373
training loss: 0.07758, validation loss: 2.12582
accuracy: 0.93171, macro average f1 score: 0.93499
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.91837   0.93750   0.92784        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.93211   0.93171   0.93169       205

recording steps without improvement: 45
----------------------------------------------------
epoch: 63, step: 379
training loss: 0.07892, validation loss: 2.12448
accuracy: 0.93171, macro average f1 score: 0.93499
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.91837   0.93750   0.92784        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.93211   0.93171   0.93169       205

recording steps without improvement: 46
----------------------------------------------------
epoch: 64, step: 385
training loss: 0.06404, validation loss: 2.15071
accuracy: 0.93171, macro average f1 score: 0.93499
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.91837   0.93750   0.92784        96
          2    0.92857   0.96296   0.94545        27

avg / total    0.93211   0.93171   0.93169       205

recording steps without improvement: 47
----------------------------------------------------
epoch: 65, step: 391
training loss: 0.06179, validation loss: 2.18646
accuracy: 0.92683, macro average f1 score: 0.92689
classification report:
             precision    recall  f1-score   support

          0    0.94937   0.91463   0.93168        82
          1    0.90909   0.93750   0.92308        96
          2    0.92593   0.92593   0.92593        27

avg / total    0.92742   0.92683   0.92689       205

recording steps without improvement: 48
----------------------------------------------------
epoch: 66, step: 397
training loss: 0.09872, validation loss: 2.22352
accuracy: 0.92195, macro average f1 score: 0.91906
classification report:
             precision    recall  f1-score   support

          0    0.94872   0.90244   0.92500        82
          1    0.90909   0.93750   0.92308        96
          2    0.89286   0.92593   0.90909        27

avg / total    0.92280   0.92195   0.92200       205

recording steps without improvement: 49
----------------------------------------------------
epoch: 67, step: 403
training loss: 0.05929, validation loss: 2.23549
accuracy: 0.92683, macro average f1 score: 0.92714
classification report:
             precision    recall  f1-score   support

          0    0.94872   0.90244   0.92500        82
          1    0.91837   0.93750   0.92784        96
          2    0.89655   0.96296   0.92857        27

avg / total    0.92763   0.92683   0.92680       205

recording steps without improvement: 50
----------------------------------------------------
test set evaluation
accuracy: 0.93046, macro average f1 score: 0.92308
classification report:
             precision    recall  f1-score   support

          0    0.93151   0.92763   0.92957      5997
          1    0.94958   0.93479   0.94213      6548
          2    0.87171   0.92496   0.89755      2079

avg / total    0.93110   0.93046   0.93064     14624
confusion matrix:
[[5563  248  186]
 [ 330 6121   97]
 [  79   77 1923]]
