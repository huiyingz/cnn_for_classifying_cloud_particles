Data:
All datapoints of the plot can be found in data.mat. The field "dt" stands
for decision tree, "SVM" for support vector machine, DCNN for the not fine-
tuned DCNN.

Plot: 
Run "plot_single_same.m" to load data and plot the figure

Get data:
"get_data_single_same.m" extracts the data from ...\Data\experiment_logfiles
for the CNN and from ..\Data\predictions\Tree_SVM for the decision tree and SVM