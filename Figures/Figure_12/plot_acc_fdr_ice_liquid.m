%% Plot ice crystals without fine-tuning

%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

load('pred_FT')
load('pred_normal')


%x-label
x_lab = 'Major axis size (�m)';

%Fontsize
fs = 20;
r = 9;
c = 2;
filenames = fieldnames(pred_normal);
l = length(filenames);
%Bin sizes
bins = pred_normal.(filenames{1}).bins;
maxsize = bins(size(bins,1),2);

%Number of size bins
N = size(bins,1);

%Calculate the interval ranges
s_interval = (log(maxsize)-log(bins(1,1)))/N;

%lines for size bins
lines = zeros(length(bins)+1,2);
lines(1:length(bins),1) = bins(:,1);
lines(1:length(bins),2) = bins(:,1);
lines(length(lines),:) = bins(length(bins),2);


s = (log(maxsize)-log(bins(1,1)))/(N*2);
%positions of size bin labels
pos =[];
for p = 1:N
    pos = [pos;exp(log(lines(p,1))+s)];
end

%% names are not general
names = {'25-47'; '47-89'; '89-168'; '168-318';'318-600'};
datasets = {'iHOLIMO 3G'; 'iHOLIMO 3M'; 'JFJ 2016'; 'SON 2016'; 'SON 2017'};
x = [bins(1,1),bins(N,2)];
y = [0,100];
y_line = [100,100];

sca = 100;


close all
%%Without fine-tuning
subplot(r,c,[1 3])
metric = 'FDR';
label = 'Ice';
for cnt = 1:length(filenames)
    scatter(pred_normal.(filenames{cnt}).(label).x,pred_normal.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
title('(a) Ice crystals without fine-tuning')
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end
ylabel('FDR (%)','FontSize',fs)

subplot(r,c,[5 7])
metric = 'accuracy';
label = 'Ice';
for cnt = 1:length(filenames)
    scatter(pred_normal.(filenames{cnt}).(label).x,pred_normal.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
ylabel('Accuracy (%)','FontSize',fs)
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
xlabel(x_lab,'FontSize',fs)
set(gca,'xtick',pos,'xticklabel',names,'XScale','log');
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end

subplot(r,c,[11 13])
metric = 'FDR';
label = 'Droplet';
for cnt = 1:length(filenames)
    scatter(pred_normal.(filenames{cnt}).(label).x,pred_normal.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
ylabel('FDR (%)','FontSize',fs)
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
title('(c) Liquid droplets without fine-tuning')
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end

subplot(r,c,[15 17])
metric = 'accuracy';
label = 'Droplet';
for cnt = 1:length(filenames)
    scatter(pred_normal.(filenames{cnt}).(label).x,pred_normal.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
ylabel('Accuracy (%)','FontSize',fs)
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
xlabel(x_lab,'FontSize',fs)
set(gca,'xtick',pos,'xticklabel',names,'XScale','log');
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end



%%With fine-tuning
subplot(r,c,[2 4])
metric = 'FDR';
label = 'Ice';
for cnt = 1:length(filenames)
    scatter(pred_FT.(filenames{cnt}).(label).x,pred_FT.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
ylabel('FDR (%)','FontSize',fs)
title('(b) Ice crystals with fine-tuning')
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end
ylabel('FDR (%)','FontSize',fs)

subplot(r,c,[6 8])
metric = 'accuracy';
label = 'Ice';
for cnt = 1:length(filenames)
    scatter(pred_FT.(filenames{cnt}).(label).x,pred_FT.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
ylabel('Accuracy (%)','FontSize',fs)
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
xlabel(x_lab,'FontSize',fs)
set(gca,'xtick',pos,'xticklabel',names,'XScale','log');
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end

subplot(r,c,[12 14])
metric = 'FDR';
label = 'Droplet';
for cnt = 1:length(filenames)
    scatter(pred_FT.(filenames{cnt}).(label).x,pred_FT.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
ylabel('FDR (%)','FontSize',fs)
title('(d) Liquid droplets with fine-tuning')
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end

subplot(r,c,[16 18])
metric = 'accuracy';
label = 'Droplet';
for cnt = 1:length(filenames)
    scatter(pred_FT.(filenames{cnt}).(label).x,pred_FT.(filenames{cnt}).(label).(metric)*sca,'filled');
    hold on
end
xlim(x)
ylim([0,100])
ylabel('Accuracy (%)','FontSize',fs)
set(gca,'XScale','log','xtick',[]);
set(gca,'XMinorTick','off','XScale','log','FontSize',fs);
xlabel(x_lab,'FontSize',fs)
set(gca,'xtick',pos,'xticklabel',names,'XScale','log');
hold on
plot(x,y_line,'k')
for p =2:N+1
    hold on
    plot(lines(p,:),y,'k')
end

legend(datasets,'Location','southoutside','orientation','horizontal')

clear bins c datasets folder_root fs names pos r s