%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path


csv_folder = fullfile(folder_root,'\Data\predictions\CNN_merged_normal\*.csv');
dataset_folder = fullfile(folder_root,'\Data\input_data\*.mat');
data_normal = get_metrics_from_csv_folder(csv_folder,dataset_folder);
csv_folder = fullfile(folder_root,'\Data\predictions\CNN_merged_FT\256\*.csv');
data_FT = get_metrics_from_csv_folder(csv_folder,dataset_folder);
data_normal = get_features_from_prtclID(data_normal);
data_FT = get_features_from_prtclID(data_FT);
data_normal = get_size_bins(data_normal,25,600,5,'majsiz');
data_FT = get_size_bins(data_FT,25,600,5,'majsiz');
pred_normal = get_plotting_data(data_normal);
pred_FT = get_plotting_data(data_FT);

clear csv_folder folder_root dataset_folder
