Data:
The datapoints for the plot can be found in "pred_FT.mat" for the results
after fine-tuning and in "pred_normal.mat" for the results before fine-tuning.
Exmaple: pred_FT.HOL3G.Droplet.accuracy contains 18 values, the first three 
belonging to the first size bin (<25�m not included), the second three to 
the second one (25-47�m) etc.. pred_FT.HOL3G.Droplet.x shows the position of
the datapoint in the plot. If NaN the datapoint is not shown because there were
less than 30 particles of the according class in the according size bin.

Plot: plot_acc_fdr_ice_liquid.m reads in pred_FT and pred_normal and creates
the shown figure in the paper.
plot_distribution_over_size plots the pie charts of the amount belonging to each
class in each shown size bin.

Get data: get_data_uncertainty_over_size reads in the results of the predictions
for the normal and fine-tuning experiments from .../Data/predicition_paper