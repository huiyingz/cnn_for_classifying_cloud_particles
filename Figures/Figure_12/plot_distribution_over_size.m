%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

load('data_normal')
datasets = fieldnames(data_normal);

%Bin sizes
bins = data_normal.(datasets{1}).sizeBins.bins;
maxsize = bins(size(bins,1),2);
N = size(bins,1);

x = [bins(1,1),maxsize];
y = [0,100];
y_line = [100,100];
% filenames = fieldnames(data_normal);
sca = 100;
ms = 100;
%Fontsize
fs = 15;
%colors
col = {'rx','gx','bx','mx','kx'}; %colors

%lines for size bins
lines = zeros(length(bins)+1,2);
lines(1:length(bins),1) = bins(:,1);
lines(1:length(bins),2) = bins(:,1);
lines(length(lines),:) = bins(length(bins),2);

%stepsize
s = (log(maxsize)-log(bins(1,1)))/(N*2);

close all
n=1;

names = {'25-47�m'; '47-89�m'; '89-168�m'; '168-318�m';'318-600�m'};
for cnt = 1:N
    bin = strcat('b',int2str(cnt));
    data.Artifact.(bin) = 0;
    data.Droplet.(bin) = 0;
    data.Ice.(bin) = 0;
    for cnt_d = 1:length(datasets)
        data.Artifact.(bin) = data.Artifact.(bin) + data_normal.(datasets{cnt_d}).sizeBins.r1.Artifact.(bin);
        data.Droplet.(bin) = data.Droplet.(bin) + data_normal.(datasets{cnt_d}).sizeBins.r1.Droplet.(bin);
        data.Ice.(bin) = data.Ice.(bin) + data_normal.(datasets{cnt_d}).sizeBins.r1.Ice.(bin);
    end
    X.(bin) = [data.Droplet.(bin),data.Artifact.(bin),data.Ice.(bin)];
    labels = {int2str(X.(bin)(1)),int2str(X.(bin)(2)),int2str(X.(bin)(3))};
    subplot(1,5,n)
    ax1 = pie(X.(bin),labels);
    %     colormap([0.6 0.2 0;0 0 1;.4 .4 .4]);%red, blue, grey
    title(names{cnt},'Fontsize',fs)
    h = legend({'Liquid','Artifact','Ice'},'location','southoutside','Orientation','horizontal','FontSize',fs);
    if cnt ~= 2
        set(h,'visible','off')
    end
    n = n+1;
end
