Data:
All datapoints of the plot can be found in data.mat. The field "dt" stands
for decision tree, "SVM" for support vector machine, DCNN for the not fine-
tuned DCNN and "fine_tuned" for the fine-tuned DCNN with 256 samples

Plot: 
Run "plot_single_different.m" to load data and plot the figure

Get data:
"get_data_single_different.m" extracts the data from ...\Data\experiment_logfiles
for the CNN and from ..\Data\predictions\Tree_SVM for the decision tree and SVM