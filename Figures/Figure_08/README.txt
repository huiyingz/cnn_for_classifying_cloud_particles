Data:
The datapoints of the plot can be found in data_FT (fine-tuning results
for different sample sizes), data_samples (results of model trained from
scratch with different sample sizes) and data_normal (trained on merged datasets
and tested on a fifth dataset not used for training)

Plot: 
Run "plot_fine_tuning_one_panel.m" to plot the figure. It will load 
data_FT, data_samples and data_normal to get the datapoints.

Get data:
"get_data_fine_tuning.m" extracts the data from ...\Data\experiment_logfiles