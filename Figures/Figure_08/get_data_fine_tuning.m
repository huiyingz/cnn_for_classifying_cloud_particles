%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path


%fine-tuning results
M_FT = get_confusion_matrices_from_txt_files(fullfile(folder_root,'Data\experiment_logfiles\FT_merged'));
data_FT = get_metrics_from_matrices_folders(M_FT);

%Results for training from scratch
M_samples = get_confusion_matrices_from_txt_files(fullfile(folder_root,'Data\experiment_logfiles\VGG_samples'));
data_samples = get_metrics_from_matrices_folders(M_samples);

%Results for normal training on merged datasets
M_normal = get_confusion_matrices_from_txt_files(fullfile(folder_root,'Data\experiment_logfiles\VGG_merged'));
data_normal = get_metrics_from_matrices_folders(M_normal);

clear folder_root M_FT M_normal M_samples