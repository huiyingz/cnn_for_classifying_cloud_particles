%Plots the fine-tuning results for different sample sizes being used for
%fine-tuning on the iHOLIMO-3G dataset on a model being trained on the
%other four datasets

%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

close all;

loc = 'Northeast';
sca = 100;
%markersize
ms = 90;
datasets = {'HOL3G'};
%Fieldnames starting with a number are not valid
datasets_new = {'HOL3G'};
fs = 15;


load('data_FT')
load('data_samples')
load('data_normal')
%%
%number: sample size
%Particles: water droplets and ice particles
leg = {'Fine-tuned DCNN', 'DCNN (from scratch)', 'DCNN (merged)'};
xlab = 'Sample size';
numbers = [64,128,256,512,1024,2048]';
x_norm = [64,2048;64,2048;64,2048];

normal_fields = fieldnames(data_normal);


%%First figure
metric = 'accuracy';
particle = []; %Leave empty if you want to look at overall metric
ylab = 'Accuracy (%)';

%Different metrics for subplots
y_FT = get_x_y_values(metric,particle,data_FT,datasets,datasets_new);
y_samples = get_x_y_values(metric,particle,data_samples,datasets,datasets_new);

cnt = 1;
if isempty(particle)
    y_norm = [repmat(data_normal.(normal_fields{cnt}).(metric)(1),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(metric)(2),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(metric)(3),1,2)];
else
    y_norm = [repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(1),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(2),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(3),1,2)];
end

subplot(1,3,1)

%%make sure sample sizes are in right order
scatter(y_FT.(datasets_new{cnt}).x,y_FT.(datasets_new{cnt}).y*sca,ms,'b*','MarkerFaceColor','b')
%ticks only at defined sample size
set(gca,'xtick',numbers,'XScale','log','FontSize',fs);
xlim([min(numbers) max(numbers)])
hold on
scatter(y_samples.(datasets_new{cnt}).x,y_samples.(datasets_new{cnt}).y*sca,ms,'r','MarkerFaceColor','r')
%ylim([0.8 1])
hold on
plot(x_norm(1,:),y_norm(1,:)*sca,'b')
hold on
plot(x_norm(2,:),y_norm(2,:)*sca,'b')
hold on
plot(x_norm(3,:),y_norm(3,:)*sca,'b')
xlabel(xlab,'Fontsize',fs)
ylabel(ylab,'Fontsize',fs)
legend(leg,'Location','southoutside','orientation','horizontal')
title('(a) Overall accuracy')

%%Second figure
metric = 'FDR';
particle = 'Droplet'; %Leave empty if you want to look at overall metric
ylab = 'FDR (%)';


%Different metrics for subplots
y_FT = get_x_y_values(metric,particle,data_FT,datasets,datasets_new);
y_samples = get_x_y_values(metric,particle,data_samples,datasets,datasets_new);

cnt = 1;
if isempty(particle)
    y_norm = [repmat(data_normal.(normal_fields{cnt}).(metric)(1),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(metric)(2),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(metric)(3),1,2)];
else
    y_norm = [repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(1),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(2),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(3),1,2)];
end

subplot(1,3,2)

%%make sure sample sizes are in right order
scatter(y_FT.(datasets_new{cnt}).x,y_FT.(datasets_new{cnt}).y*sca,ms,'b*','MarkerFaceColor','b')
%ticks only at defined sample size
set(gca,'xtick',numbers,'XScale','log','FontSize',fs);
xlim([min(numbers) max(numbers)])
hold on
scatter(y_samples.(datasets_new{cnt}).x,y_samples.(datasets_new{cnt}).y*sca,ms,'r','MarkerFaceColor','r')
%ylim([0.8 1])
hold on
plot(x_norm(1,:),y_norm(1,:)*sca,'b')
hold on
plot(x_norm(2,:),y_norm(2,:)*sca,'b')
hold on
plot(x_norm(3,:),y_norm(3,:)*sca,'b')
xlabel(xlab,'Fontsize',fs)
ylabel(ylab,'Fontsize',fs)
title('(b) FDR of liquid droplets')

%%Third figure
metric = 'FDR';
particle = 'Ice'; %Leave empty if you want to look at overall metric
ylab = 'FDR (%)';
% if ~isempty(particle)
%     name = strcat(particle,'_',metric);
% else
%     name = metric;
% end

%Different metrics for subplots
y_FT = get_x_y_values(metric,particle,data_FT,datasets,datasets_new);
y_samples = get_x_y_values(metric,particle,data_samples,datasets,datasets_new);

cnt = 1;
if isempty(particle)
    y_norm = [repmat(data_normal.(normal_fields{cnt}).(metric)(1),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(metric)(2),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(metric)(3),1,2)];
else
    y_norm = [repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(1),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(2),1,2);...
        repmat(data_normal.(normal_fields{cnt}).(particle).(metric)(3),1,2)];
end

subplot(1,3,3)

%%make sure sample sizes are in right order
scatter(y_FT.(datasets_new{cnt}).x,y_FT.(datasets_new{cnt}).y*sca,ms,'b*','MarkerFaceColor','b')
%ticks only at defined sample size
set(gca,'xtick',numbers,'XScale','log','FontSize',fs);
xlim([min(numbers) max(numbers)])
hold on
scatter(y_samples.(datasets_new{cnt}).x,y_samples.(datasets_new{cnt}).y*sca,ms,'r','MarkerFaceColor','r')
%ylim([0.8 1])
hold on
plot(x_norm(1,:),y_norm(1,:)*sca,'b')
hold on
plot(x_norm(2,:),y_norm(2,:)*sca,'b')
hold on
plot(x_norm(3,:),y_norm(3,:)*sca,'b')
% legend(leg,'Location',loc)
xlabel(xlab,'Fontsize',fs)
ylabel(ylab,'Fontsize',fs)
title('(c) FDR of ice crystals')


