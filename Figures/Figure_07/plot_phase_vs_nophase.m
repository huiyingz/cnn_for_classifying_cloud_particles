%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path folder_root

load('data')

close all;
[ax1,ax2,ax3,ax4] = plot_tables(data);
f = 0.4;
a = 1;
x = [0,4];
yline_f = [f,f];
yline_a = [a,a];
ylim(ax3,[0,f])
ylim(ax4,[0.8,a])

%set(h,'FontSize',30)

plot(ax3,x,yline_f,'k')
plot(ax4,x,yline_a,'k')

[h,icon] = legend(ax3,'without phase','with phase','Location','bestoutside');
M = findobj(icon,'type','patch') % Find objects of type 'patch'
set(M,'MarkerSize', sqrt(500))
h = legend(ax4,'without phase','with phase','Location','bestoutside');
h = legend(ax4,'hide');
