Data:
All datapoints of the plot can be found in data.mat. The field "nophase" stands
for CNN run using only the amplitude images and "phase" for using both the amplitude
and phase images

Plot: 
Run "plot_phase_vs_nophase.m" to load data.mat and plot the figure

Get data:
"get_data_phase_vs_nophase.m" extracts the data from ../Data/2016_iHOLIMO_experiments