# Deep Learning for Analyzing Atmospheric Clouds
Author: George Touloupas (g.touloupas@gmail.com)

## Prerequisites
1. Python 3 (preferably 3.5 or 3.6, at least 3.4) must be installed and accessed from the working directory with the command python.
2. The working directory should be the pipeline folder when executing the following commands.
3. The required packages must be installed to python 3 using one of the following ways, in order of preference for a deterministic build:
    * (Recommended) Creates a virtual environment for the pipeline folder and installs the required packages in the created environment:
    First install pipenv with "pip install  --user --upgrade pipenv". Then the requirements can be installed as the current user (not the administrator)
    with "pipenv install".
    * Installs the recommended versions of the required packages for the current user: pip install --user -r requirements.txt
    * (Not recommended) Installs the newest versions of the required packages for the current user, may break functionality:
    pip install --user --upgrade scipy numpy pandas Pillow scikit-learn tensorflow
    * (GPU support) To install TensorFlow with GPU support refer to the official installation guides at https://www.tensorflow.org/install.
4. If the required packages were installed with pipenv:
    * The command "pipenv shell" must be run one time before any of the following commands to use the virtual environment.
    * Otherwise the following commands can be prepended with "pipenv run ".
5. All MAT-files should be included in the data directory.
6. All trained models should be included in the models directory.

## Notes
* The dcnn_pipeline directory and its contents are deleted every time a pipeline script is run.
* For more information about a script's options, use the -h flag: python <script>.py -h

## Making predictions
python predict_pipeline.py --mat_files data/input_NN.mat --model_dir models/dcnn_model_res_BN_aug_merged_all

* The dcnn_model_res_BN_aug_merged_all model is trained on all 5 datasets (90-10 training-validation split).
* The prediction file dcnn_pred_pipeline.csv can be found in the dcnn_pipeline directory.
* The batch_size can be increased according to the RAM/GPU memory available to speed up the predictions.
* If batch normalization was not used during training, the --no_batchnorm flag must be set.

## Training a model from scratch
python train_pipeline.py --mat_files data/input_NN.mat --suffix input_NN

* Use a different suffix for each model in order to not overwrite a previous one.
* The batch_size can be increased according to the RAM/GPU memory available to speed up training.
* There are options to disable data augmentation or batch normalization to speed up the experiments.
* The trained model can be found in the models directory.
* The trained model logs can be found in the dcnn_pipeline directory.
* If the logs indicate that the training or validation loss is nan, then the network has not trained correctly. Retrain using more data.

## Fine-tuning a model
python train_pipeline.py --mat_files data/input_NN.mat --resume_training --model_dir models/dcnn_model_res_BN_aug_merged_all --early_stopping_epochs 200 --suffix input_NN_FT

* Use a different suffix for each model in order to not overwrite a previous one.
* The batch_size can be increased according to the RAM/GPU memory available to speed up training.
* There are options to disable data augmentation or batch normalization to speed up fine-tuning. If batch normalization was used for the pretrained model, then it must not be disabled during fine-tuning.
* The learning_rate and max_epochs are automatically set to 0.0001 and 5000 accordingly.
* The early_stopping_epochs option should be increased if the training/validation set is small. A value of 200 is good for 256 training/validation samples, while a value of 10 is good for 4k+ samples.
* The fine-tuned model can be found in the models directory.
* The fine-tuned model logs can be found in the dcnn_pipeline directory.
* If the logs indicate that the training or validation loss is nan, then the network has not fine-tuned correctly. Fine-tune the pretrained model again using more data.
