"""Execute the full pipeline for predictions."""
import argparse
import os
import shutil
import subprocess

PIPELINE_DIR = "dcnn_pipeline"  # Directory for intermediate and final results


def create_empty_dir(dir_):
    """Create an empty directory."""
    os.makedirs(dir_, exist_ok=True)
    shutil.rmtree(dir_)
    os.makedirs(dir_, exist_ok=True)


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--mat_files", nargs="+", type=str, help="MAT-files to be preprocessed")
    parser.add_argument("--model_dir", type=str, help="directory containing the model")
    parser.add_argument("--batch_size", type=int, default=256, help="number of images per batch")
    parser.add_argument("--no_batchnorm", action="store_true", help="disable batch normalization")
    
    return parser.parse_args()


def main(args):
    if not args.mat_files:
        print("No MAT-files files were provided")
        exit(-1)
    create_empty_dir(PIPELINE_DIR)

    extra_flags = ""
    if not args.no_batchnorm:
        extra_flags += "--batchnorm "
    
    # Preprocess the data
    cmd = "python preprocessing.py --mat_files {} --prep_dir {} --suffix pipeline".format(" ".join(args.mat_files),
                                                                                          PIPELINE_DIR)
    subprocess.run(cmd, shell=True)
    
    # Make predictions
    cmd = "python dcnn.py --predict --pred_set {}/dataset_pipeline.pkl --model_dir {} --log_dir {} --pred_dir {} " \
          "--batch_size {} {} --residual --suffix pipeline".format(PIPELINE_DIR, args.model_dir, PIPELINE_DIR,
                                                                   PIPELINE_DIR, args.batch_size, extra_flags)
    subprocess.run(cmd, shell=True)


if __name__ == "__main__":
    main(parse_args())
