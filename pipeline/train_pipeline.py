"""Execute the full pipeline for training/fine-tuning."""
import argparse
import os
import shutil
import subprocess

PIPELINE_DIR = "dcnn_pipeline"  # Directory for intermediate and final results


def create_empty_dir(dir_):
    """Create an empty directory."""
    os.makedirs(dir_, exist_ok=True)
    shutil.rmtree(dir_)
    os.makedirs(dir_, exist_ok=True)


def parse_args():
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument("--mat_files", nargs="+", type=str, help="MAT-files to be preprocessed")
    parser.add_argument("--val_pct", type=float, default=0.2, help="percentage of data used for the validation set")
    parser.add_argument("--batch_size", type=int, default=256, help="number of images per batch")
    parser.add_argument("--suffix", type=str, help="suffix for the created model directory")
    parser.add_argument("--resume_training", action="store_true",
                        help="restore the model and resume training (fine-tuning)")
    parser.add_argument("--model_dir", type=str, default="models", help="directory containing the pretrained model")
    parser.add_argument("--early_stopping_epochs", type=int, default=10,
                        help="number of epochs without improvement to stop training, -1 disables early stopping, "
                             "during fine-tuning it is recommended to change it to 200 for a small training set of "
                             "~256 samples")
    parser.add_argument("--no_augment", action="store_true", help="disable data augmentation")
    parser.add_argument("--no_batchnorm", action="store_true", help="disable batch normalization")
    
    return parser.parse_args()


def main(args):
    if not args.mat_files:
        print("No MAT-files files were provided")
        exit(-1)
    if args.suffix is None:
        args.suffix = ""
    create_empty_dir(PIPELINE_DIR)
    
    extra_flags = ""
    if args.resume_training:
        extra_flags += "--resume_training --learning_rate 0.0001 --max_epochs 5000 "
    if not args.no_augment:
        extra_flags += "--augment "
    if not args.no_batchnorm:
        extra_flags += "--batchnorm "
    
    # Preprocess the data
    cmd = "python preprocessing.py --mat_files {} --prep_dir {} --split --val_pct {} --test_pct 0 " \
          "--suffix pipeline".format(" ".join(args.mat_files), PIPELINE_DIR, args.val_pct)
    subprocess.run(cmd, shell=True)
    
    # Train/fine-tune the model
    cmd = "python dcnn.py --train --val --train_set {}/train_pipeline.pkl --val_set {}/val_pipeline.pkl " \
          "--model_dir {} --log_dir {} --batch_size {} --early_stopping_epochs {} {} --residual --suffix {}".format(
               PIPELINE_DIR, PIPELINE_DIR, args.model_dir, PIPELINE_DIR, args.batch_size, args.early_stopping_epochs,
               extra_flags, args.suffix)
    subprocess.run(cmd, shell=True)


if __name__ == "__main__":
    main(parse_args())
