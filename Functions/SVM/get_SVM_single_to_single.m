%Create SVMs on all single datasets available
%kfold = 4
%penalizes by inverse class frequency of smaller class
%60-20-20 split
kfold = 4;

%Add Folder Functions which is a subfolder of
%CNN_for_Classifying_Cloud_Particles folder
current_path = pwd;
main_folder_name_position = strfind(current_path,'CNN_for_Classifying_Cloud_Particles');
folder_root = current_path(1:main_folder_name_position+34);
folder_path = strcat(folder_root,'\Functions');
addpath(genpath(folder_path));
clear current_path main_folder_name_position folder_path

%seen: data to train on and validate on
datasets = fullfile(folder_root,'\Data\input_data\*.mat');
dataset_list = dir(datasets);
dataset_folder = {dataset_list.folder};
datasets = {dataset_list.name};
clear dataset_list

for cnt = 1:length(datasets)
    name = strsplit(datasets{cnt},'.');
    
    load(fullfile(dataset_folder{1},datasets{cnt}))% has to be temp
    
    %%split data
    N = length(temp.class);
    Ntest = round(length(temp.class)/5); %Test set will not be used for training and validation
    k = randperm(N);
    temp_new = temp;
    temp_new.metricmat = temp.metricmat(k,:);
    temp_new.class = temp.class(k,:);
    temp_new.cpType = temp.cpType(k,:);
    temp_new.prtclID = temp.prtclID(k,:);
    temp_new.prtclIm = temp.prtclIm(k,:);
    testset = temp_new;
    testset.metricmat = temp_new.metricmat(1:Ntest,:);
    testset.class = temp_new.class(1:Ntest,:);
    testset.cpType = temp_new.cpType(1:Ntest,:);
    testset.prtclID = temp_new.prtclID(1:Ntest,:);
    testset.prtclIm = temp_new.prtclIm(1:Ntest,:);
    temp_new.metricmat(1:Ntest,:) = [];
    temp_new.class(1:Ntest,:) = [];    
    
    Ntrain = length(temp_new.class);
    Nvali = round(Ntrain/kfold);
    
    %training set
    train = temp_new;
    %validation set
    vali = temp_new;
    
    accuracyCost = 0;
    
    %Create kfold sets of validation set and training set
    for j = 1:kfold
        if j == 1
            vali.metricmat = temp_new.metricmat(1:Nvali,:);
            vali.class =temp_new.class(1:Nvali,:);
            train.metricmat = temp_new.metricmat(Nvali+1:Ntrain,:);
            train.class = temp_new.class(Nvali+1:Ntrain,:);
        elseif j == kfold
            vali.metricmat = temp_new.metricmat((j-1)*Nvali+1:Ntrain,:);
            vali.class = temp_new.class((j-1)*Nvali+1:Ntrain,:);
            train.metricmat = temp_new.metricmat(1:(j-1)*Nvali,:);
            train.class = temp_new.class(1:(j-1)*Nvali,:);
        else
            vali.metricmat = temp_new.metricmat((j-1)*Nvali+1:j*Nvali,:);
            vali.class = temp_new.class((j-1)*Nvali+1:j*Nvali,:);
            train.metricmat = temp_new.metricmat(1:(j-1)*Nvali,:);
            train.metricmat((j-1)*Nvali+1:Ntrain-Nvali,:) = temp_new.metricmat(j*Nvali+1:Ntrain,:);
            train.class = temp_new.class(1:(j-1)*Nvali,:);
            train.class((j-1)*Nvali+1:Ntrain-Nvali,:) = temp_new.class(j*Nvali+1:Ntrain,:);
        end
        
        BC = 11;        
        for bc = 1:BC
        %Create SVM with training data and box constraint value
        [SVMFileCost] = createSVM(train,bc);
        
        %Predict on validation set
        dataCostSVM = SVMPrediction(vali,SVMFileCost);
       
        
        %Save best SVM and best box constraint value
        if dataCostSVM.accuracy>accuracyCost
            bestCostSVM = SVMFileCost;
            bestCostBc = bc;
            accuracyCost = dataCostSVM.accuracy;
        end
        end
    end
    clear data
    SVM = bestCostSVM;
    data = SVMPrediction(testset,SVM);
    data.bc = bestCostBc;
    data.accuracy_validation = accuracyCost;
    data.prtclIm = [];
    save(strcat(name{1},'_single_SVM'),'SVM')
    save(strcat(name{1},'_SVM_predicted'),'data')
end


