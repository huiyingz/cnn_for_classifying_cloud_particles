function [SVMFileCost] = createSVM(inputData,bc)

classData = inputData;
%make sure the categories of the classData are correct
x = cellstr(classData.class);
classData.class = categorical(x);
clear x

total = length(classData.class);
classData.artifact = find(classData.class == 'Artifact');
classData.artifactFrequency = length(classData.artifact)/total;
classData.ice = find(classData.class == 'Particle_nubbly');
classData.iceFrequency = length(classData.ice)/total;
classData.water = find(classData.class == 'Particle_round');
classData.waterFrequency = length(classData.water)/total;


classData.S.ClassificationCosts = [0 1 1; 1 0 1; 1 1 0]; 

if classData.artifactFrequency < classData.iceFrequency
    classData.S.ClassificationCosts(1,2) = 1/classData.artifactFrequency;
    classData.S.ClassificationCosts(2,1) = 1/classData.artifactFrequency;
else
    classData.S.ClassificationCosts(1,2) = 1/classData.iceFrequency;
    classData.S.ClassificationCosts(2,1) = 1/classData.iceFrequency;
end

if classData.artifactFrequency < classData.waterFrequency
    classData.S.ClassificationCosts(1,3) = 1/classData.artifactFrequency;
    classData.S.ClassificationCosts(3,1) = 1/classData.artifactFrequency;
else
    classData.S.ClassificationCosts(1,3) = 1/classData.waterFrequency;
    classData.S.ClassificationCosts(3,1) = 1/classData.waterFrequency;
end

if classData.iceFrequency < classData.waterFrequency
    classData.S.ClassificationCosts(2,3) = 1/classData.iceFrequency;
    classData.S.ClassificationCosts(3,2) = 1/classData.iceFrequency;
else
    classData.S.ClassificationCosts(2,3) = 1/classData.waterFrequency;
    classData.S.ClassificationCosts(3,2) = 1/classData.waterFrequency;
end



classData.S.ClassNames = categories(classData.class);


%%SVM
template = templateSVM(...
    'KernelFunction', 'rbf', ...
    'KernelScale', 'auto', ...
    'BoxConstraint', bc, ...
    'Standardize', true);


SVMFileCost = fitcecoc(...
    classData.metricmat, ...
    classData.class, ...
    'Learners', template, ...
    'Coding', 'onevsone', ...
    'ClassNames', categories(classData.class), ...
    'PredictorNames',classData.metricnames,...
    'Cost',classData.S);


end