%Input is a struct containing fields with confusion matrices (e.g. M1, M2...)
%output is a struct containing fields (e.g. M1, M2...) now with the evaluation metrics of
%each matrix
function data = get_metrics_from_matrices(M)
fnames = fieldnames(M);

Mnames = fnames;
l = length(Mnames);

names = strings([l,1]);
for cnt = 1:l %different runs of same dataset
    newStr = erase(Mnames{cnt},'_r1');
    newStr = erase(newStr,'_r2');
    newStr = erase(newStr,'_r3');
    names{cnt} = newStr;
end
clear cnt newStr

%X: same number, same string
[D,~,X] = unique(names(:));
K=3;
for cnt = 1:length(D) %number of tests (number of runs not included)
    k = find(X==cnt);
    for i = 1:length(k) %number of runs
        CM = M.(Mnames{k(i)});
        N = sum(sum(CM));
        data.(names{k(1)}).accuracy(i,1) = trace(CM)/N;
        s = 0;
        for j = 1:K
            s = s+sum(CM(:,j))*sum(CM(j,:));
        end
        E = s/(N^2);
        data.(names{k(1)}).HSS(i,1) = (data.(names{k(1)}).accuracy(i,1)-E)/(1-E);
        data.(names{k(1)}).Artifact.accuracy(i,1) = CM(1,1)/sum(CM(1,:));
        data.(names{k(1)}).Artifact.FDR(i,1) = (sum(CM(:,1))-CM(1,1))/sum(CM(:,1));
        data.(names{k(1)}).Droplet.accuracy(i,1) = CM(2,2)/sum(CM(2,:));
        data.(names{k(1)}).Droplet.FDR(i,1) = (sum(CM(:,2))-CM(2,2))/sum(CM(:,2));
        data.(names{k(1)}).Ice.accuracy(i,1) = CM(3,3)/sum(CM(3,:));
        data.(names{k(1)}).Ice.FDR(i,1) = (sum(CM(:,3))-CM(3,3))/sum(CM(:,3));
        data.(names{k(1)}).Particles.accuracy(i,1) = (CM(3,3)+CM(2,2))/sum(sum(CM(2:3,:)));
        data.(names{k(1)}).Particles.FDR(i,1) = (sum(sum(CM(:,2:3)))-CM(3,3)-CM(2,2))/sum(sum(CM(:,2:3)));
        data.(names{k(1)}).BER(i,1) = (3-data.(names{k(1)}).Artifact.accuracy(i,1)...
            -data.(names{k(1)}).Droplet.accuracy(i,1)-data.(names{k(1)}).Ice.accuracy(i,1))/3;
    end
        data.(names{k(1)}).Artifact.accuracy(isnan(data.(names{k(1)}).Artifact.accuracy)) = 1;
        data.(names{k(1)}).Artifact.FDR(isnan(data.(names{k(1)}).Artifact.FDR)) = 0;
        data.(names{k(1)}).Droplet.accuracy(isnan(data.(names{k(1)}).Droplet.accuracy)) = 1;
        data.(names{k(1)}).Droplet.FDR(isnan(data.(names{k(1)}).Droplet.FDR)) = 0;
        data.(names{k(1)}).Ice.accuracy(isnan(data.(names{k(1)}).Ice.accuracy)) = 1;
        data.(names{k(1)}).Ice.FDR(isnan(data.(names{k(1)}).Ice.FDR)) = 0;
        data.(names{k(1)}).Particles.accuracy(isnan(data.(names{k(1)}).Particles.accuracy)) = 1;
        data.(names{k(1)}).Particles.FDR(isnan(data.(names{k(1)}).Particles.FDR)) = 0;
        data.(names{k(1)}).accuracy(isnan(data.(names{k(1)}).accuracy)) = 1;
end


