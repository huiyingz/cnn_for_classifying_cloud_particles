function classData = merge_datasets(folder,excluded_set)

filelist = dir(fullfile(folder,'*.mat'));
filenames = {filelist.name};
name = strsplit(excluded_set,'.');
unused = find(contains(filenames,name{1}));
filenames(unused) = [];
l = length(filenames);
clear filelist unused

for i = 1:l
        temp = load(fullfile(folder,filenames{i}));
        classDataIn= temp.temp;
        if i == 1
            classData.prtclID = classDataIn.prtclID;
            classData.metricmat = classDataIn.metricmat;
            classData.metricnames = classDataIn.metricnames;
            x =  cellstr(classDataIn.class);
            classDataIn.class = categorical(x);
            clear x;
            classData.class = classDataIn.class;
            classData.cpType = classDataIn.cpType;
        else
            %choose only Predictor that exist in every _class file
            [classData.metricnames, ia, ib] = ...
                intersect(classData.metricnames, classDataIn.metricnames);
            x =  cellstr(classDataIn.class);
            classDataIn.class = categorical(x);
            clear x;
            classData.metricmat = [classData.metricmat(:,ia); classDataIn.metricmat(:,ib)];
            classData.class = [classData.class; classDataIn.class];
            classData.cpType = [classData.cpType; classDataIn.cpType];
            classData.prtclID = [classData.prtclID; classDataIn.prtclID];
        end
    end
end

