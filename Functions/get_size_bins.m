%Input: struct with fields of datasets including different runs (e.g.
%data.HOL3G.r1, the lowest and highest number of the considered position (minsize, maxsize),
%the number of bins (N)
%Output: struct with fields of datasets now including field of sizeBins
%including the different runs (e.g. data.HOL3G.sizeBins.r1)
function data = get_size_bins(data,minsize,maxsize,N,feature)


%Calculate the interval ranges
s = (log(maxsize)-log(minsize))/N;

bins = zeros(N,2);
bins(1,1) = minsize;
bins(1,2) = round(exp(log(bins(1,1))+s));
for i = 2:N
    bins(i,1) = exp(log(bins(i-1,1))+s);
    bins(i,2) = exp(log(bins(i,1))+s);
end
bins = round(bins);

datasets = fields(data);
l = length(datasets);

for i = 1:l %Load 5 predicted datasets
    
    %Save bin limits
    data.(datasets{i}).sizeBins.bins = bins;
    
    %Not every dataset has the same amount of runs
    run(1) = isfield(data.(datasets{i}),'r1');
    run(2) = isfield(data.(datasets{i}),'r2');
    run(3) = isfield(data.(datasets{i}),'r3');
    clear r %create a variable with includes the existing runs
    %x axis: offset stepsizs (s) times i. Different for different numbers
    %of runs
    if run(1) == 1
        r{1} = strcat('r',int2str(1));
        if run(2) == 1
            r{2} = strcat('r',int2str(2));
            if run(3) == 1
                r{3} = strcat('r',int2str(3));
            end
        elseif run(3) == 1
            r{2} = strcat('r',int2str(3));
        end
    elseif run(2) == 1
        r{1} = strcat('r',int2str(2));
        if run(3) == 1
            r{2} = strcat('r',int2str(3));
        end
    elseif run(3) == 1
        r{1} = strcat('r',int2str(3));
    end
    
    
    for k = 1:sum(run) %go through number of runs
        j = strcat('r',int2str(k));
        for n = 1:N %go through bins
            b = strcat('b',int2str(n));  
            bin.(b) = find(data.(datasets{i}).(j).(feature) >= bins(n,1)...
                & data.(datasets{i}).(j).(feature) < bins(n,2));
            %Number of particles in each size bin
            data.(datasets{i}).sizeBins.(j).(b) = length(bin.(b));

            Artifact.class = intersect(data.(datasets{i}).(j).Artifact.class,bin.(b));
            Artifact.predicted = intersect(data.(datasets{i}).(j).Artifact.predicted,bin.(b));
            Artifact.true = intersect(data.(datasets{i}).(j).Artifact.true,bin.(b));
            Droplet.class = intersect(data.(datasets{i}).(j).Droplet.class,bin.(b));
            Droplet.predicted = intersect(data.(datasets{i}).(j).Droplet.predicted,bin.(b));
            Droplet.true = intersect(data.(datasets{i}).(j).Droplet.true,bin.(b));
            Ice.class = intersect(data.(datasets{i}).(j).Ice.class,bin.(b));
            Ice.predicted = intersect(data.(datasets{i}).(j).Ice.predicted,bin.(b));
            Ice.true = intersect(data.(datasets{i}).(j).Ice.true,bin.(b));
            
            data.(datasets{i}).sizeBins.(j).Artifact.(b) = length(Artifact.class);
            data.(datasets{i}).sizeBins.(j).Droplet.(b) = length(Droplet.class);
            data.(datasets{i}).sizeBins.(j).Ice.(b) = length(Ice.class);
            
            m = strcat('M',int2str(n));
            data.(datasets{i}).sizeBins.(j).(m)(1,1) = length(Artifact.true);
            data.(datasets{i}).sizeBins.(j).(m)(2,2) = length(Droplet.true);
            data.(datasets{i}).sizeBins.(j).(m)(3,3) = length(Ice.true);
            data.(datasets{i}).sizeBins.(j).(m)(1,2) = length(intersect(Artifact.class,Droplet.predicted));
            data.(datasets{i}).sizeBins.(j).(m)(1,3) = length(intersect(Artifact.class,Ice.predicted));
            data.(datasets{i}).sizeBins.(j).(m)(2,1) = length(intersect(Droplet.class,Artifact.predicted));
            data.(datasets{i}).sizeBins.(j).(m)(2,3) = length(intersect(Droplet.class,Ice.predicted));
            data.(datasets{i}).sizeBins.(j).(m)(3,1) = length(intersect(Ice.class,Artifact.predicted));
            data.(datasets{i}).sizeBins.(j).(m)(3,2) = length(intersect(Ice.class,Droplet.predicted));
            
            data.(datasets{i}).sizeBins.(j).accuracy(n) = trace(data.(datasets{i}).sizeBins.(j).(m))/sumabs(data.(datasets{i}).sizeBins.(j).(m));
            data.(datasets{i}).sizeBins.(j).accuracyParticles(n) = (data.(datasets{i}).sizeBins.(j).(m)(2,2)+data.(datasets{i}).sizeBins.(j).(m)(3,3))/...
                sumabs(data.(datasets{i}).sizeBins.(j).(m)(2:3,:));
            data.(datasets{i}).sizeBins.(j).devTruthParticles(n) = (sumabs(data.(datasets{i}).sizeBins.(j).(m)(:,2:3))-...
                sumabs(data.(datasets{i}).sizeBins.(j).(m)(2:3,:)))/sumabs(data.(datasets{i}).sizeBins.(j).(m)(2:3,:));
            data.(datasets{i}).sizeBins.(j).FDR(n) = (sumabs(data.(datasets{i}).sizeBins.(j).(m))-trace(data.(datasets{i}).sizeBins.(j).(m)))/...
                sumabs(data.(datasets{i}).sizeBins.(j).(m));
            data.(datasets{i}).sizeBins.(j).FDRParticles(n) = (sum(data.(datasets{i}).sizeBins.(j).(m)(1:2,3))+data.(datasets{i}).sizeBins.(j).(m)(1,2)+data.(datasets{i}).sizeBins.(j).(m)(3,2))/...
                sumabs(data.(datasets{i}).sizeBins.(j).(m)(:,2:3));
            data.(datasets{i}).sizeBins.(j).Artifact.accuracy(n) = data.(datasets{i}).sizeBins.(j).(m)(1,1)/sum(data.(datasets{i}).sizeBins.(j).(m)(1,:));
            data.(datasets{i}).sizeBins.(j).Artifact.precision(n) = data.(datasets{i}).sizeBins.(j).(m)(1,1)/sum(data.(datasets{i}).sizeBins.(j).(m)(:,1));
            data.(datasets{i}).sizeBins.(j).Artifact.FDR(n) = sum(data.(datasets{i}).sizeBins.(j).(m)(2:3,1))/sum(data.(datasets{i}).sizeBins.(j).(m)(:,1));
            data.(datasets{i}).sizeBins.(j).Artifact.devTruth(n) = (sum(data.(datasets{i}).sizeBins.(j).(m)(:,1))-...
                sum(data.(datasets{i}).sizeBins.(j).(m)(1,:)))/sum(data.(datasets{i}).sizeBins.(j).(m)(1,:));
            data.(datasets{i}).sizeBins.(j).Droplet.accuracy(n) = data.(datasets{i}).sizeBins.(j).(m)(2,2)/sum(data.(datasets{i}).sizeBins.(j).(m)(2,:));
            data.(datasets{i}).sizeBins.(j).Droplet.precision(n) = data.(datasets{i}).sizeBins.(j).(m)(2,2)/sum(data.(datasets{i}).sizeBins.(j).(m)(:,2));
            data.(datasets{i}).sizeBins.(j).Droplet.FDR(n) = (data.(datasets{i}).sizeBins.(j).(m)(1,2)+data.(datasets{i}).sizeBins.(j).(m)(3,2))/sum(data.(datasets{i}).sizeBins.(j).(m)(:,2));
            data.(datasets{i}).sizeBins.(j).Droplet.devTruth(n) = (sum(data.(datasets{i}).sizeBins.(j).(m)(:,2))-...
                sum(data.(datasets{i}).sizeBins.(j).(m)(2,:)))/sum(data.(datasets{i}).sizeBins.(j).(m)(2,:));
            data.(datasets{i}).sizeBins.(j).Ice.accuracy(n) = data.(datasets{i}).sizeBins.(j).(m)(3,3)/sum(data.(datasets{i}).sizeBins.(j).(m)(3,:));
            data.(datasets{i}).sizeBins.(j).Ice.precision(n) = data.(datasets{i}).sizeBins.(j).(m)(3,3)/sum(data.(datasets{i}).sizeBins.(j).(m)(:,3));
            data.(datasets{i}).sizeBins.(j).Ice.FDR(n) = sum(data.(datasets{i}).sizeBins.(j).(m)(1:2,3))/sum(data.(datasets{i}).sizeBins.(j).(m)(:,3));
            data.(datasets{i}).sizeBins.(j).Ice.devTruth(n) = (sum(data.(datasets{i}).sizeBins.(j).(m)(:,3))-...
                sum(data.(datasets{i}).sizeBins.(j).(m)(3,:)))/sum(data.(datasets{i}).sizeBins.(j).(m)(3,:));
        end
    end
end
end