%Input is a struct containing fields with matrices or a struct containing
%fields (names of different folders) which contain fields of matrices

%output is a struct containing fields (names of different folders) which 
%contain fields of the evaluation metrics of the
%matrices which were in that field with the same name


function data = get_metrics_from_matrices_folders(M)
fnames = fieldnames(M);
s = length(fnames);
%M can contain several folders with matrices or is a folder with matrices
if ~isstruct(M.(fnames{1}))
    %M is a struct with matrices names
    data = get_metrics_from_matrices(M);
else
    %M is struct with folder names containing names of matrices
    for cnt = 1:s
        data.(fnames{cnt}) = get_metrics_from_matrices(M.(fnames{cnt}));
    end
end
end