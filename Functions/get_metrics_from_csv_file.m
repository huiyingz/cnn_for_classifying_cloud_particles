%%to do: if accuracy = NaN change to 1 and if FDR = NaN change to 0

%Input is the csv_file and its according dataset
%output is a data struct with the prtclID and the metrics
function data = get_metrics_from_csv_file(csv_file,dataset)

%if only path was sent, dataset still has to be loaded
if ~isstruct(dataset)
    load(dataset); %temp
else
    temp = dataset;
end
[prtclID,predicted_class]  = textread(csv_file,'%s %d','delimiter',',');


p_majsiz = find(contains(dataset.metricnames,'majsiz'));
majsiz = dataset.metricmat(:,p_majsiz)*1e6;
clear p_majsiz
class = string(temp.cpType);
%Test before excluding fine-tuning --> saves time
if length(class) < length(prtclID)
    error('The given dataset does not correspond to the csv file!')
end
%The fine-tuning samples are not used as test
for cnt = length(temp.prtclID):-1:1
    if ~ismember(temp.prtclID{cnt},prtclID)
        class(cnt) = [];
        majsiz(cnt) = [];
    end
end

if length(class) ~= length(prtclID)
    error('The given dataset does not correspond to the csv file!')
end

%Save the prtclIDs which are used for testing to get the size out of it
data.prtclID = prtclID;
data.majsiz = majsiz;

predicted = class;
predicted(find(predicted_class==0)) = 'Artifact';
predicted(find(predicted_class==1)) = 'Particle_round';
predicted(find(predicted_class==2)) = 'Particle_nubbly';

data.Artifact.class = find(class == 'Artifact');
data.Artifact.predicted = find(predicted == 'Artifact');
data.Artifact.true = intersect(data.Artifact.class,data.Artifact.predicted);
data.Artifact.false = setdiff(data.Artifact.predicted,data.Artifact.true);
data.Artifact.missing = setdiff(data.Artifact.class,data.Artifact.predicted);
data.Artifact.accuracy = length(data.Artifact.true)/length(data.Artifact.class);
data.Artifact.precision = length(data.Artifact.true)/length(data.Artifact.predicted);

data.Droplet.class = find(class == 'Particle_round');
data.Droplet.predicted = find(predicted == 'Particle_round');
data.Droplet.true = intersect(data.Droplet.class,data.Droplet.predicted);
data.Droplet.false = setdiff(data.Droplet.predicted,data.Droplet.true);
data.Droplet.missing = setdiff(data.Droplet.class,data.Droplet.predicted);
data.Droplet.accuracy = length(data.Droplet.true)/length(data.Droplet.class);
data.Droplet.precision = length(data.Droplet.true)/length(data.Droplet.predicted);

data.Ice.class = find(class == 'Particle_nubbly');
data.Ice.predicted = find(predicted == 'Particle_nubbly');
data.Ice.true = intersect(data.Ice.class,data.Ice.predicted);
data.Ice.false = setdiff(data.Ice.predicted,data.Ice.true);
data.Ice.missing = setdiff(data.Ice.class,data.Ice.predicted);
data.Ice.accuracy = length(data.Ice.true)/length(data.Ice.class);
data.Ice.precision = length(data.Ice.true)/length(data.Ice.predicted);

data.accuracy = (length(data.Droplet.true)+length(data.Ice.true)+length(data.Artifact.true))/length(class);
%data.pruningLevel = data.accuracy;


%M: Confusion Matrix
M = zeros(3,3);
M(1,1) = length(data.Artifact.true);
M(2,2) = length(data.Droplet.true);
M(3,3) = length(data.Ice.true);
M(2,1) = length(intersect(data.Artifact.class,data.Droplet.predicted));
M(3,1) = length(intersect(data.Artifact.class,data.Ice.predicted));
M(1,2) = length(intersect(data.Droplet.class,data.Artifact.predicted));
M(3,2) = length(intersect(data.Droplet.class,data.Ice.predicted));
M(1,3) = length(intersect(data.Ice.class,data.Artifact.predicted));
M(2,3) = length(intersect(data.Ice.class,data.Droplet.predicted));

%number of classes
K = 3;

%E: Number of correct predictions that could occur by chance
s = 0;
for k = 1:K
    s = s+sum(M(:,k))*sum(M(k,:));
end

E = s/length(class)^2;

data.HSS = (data.accuracy-E)/(1-E);
%(OA-E)/1-E
%E = 1/N^2* sum_i=1^k Mi*M*i
%Mi*: total for ith row
%M*j: total for jth column
%j:labeled (columns), i: predicted (rows)

%1/K sum_k=1^K (1/Nk) sum_n=1^N (yn = k)(yn ~= y^n))
%BER also equal to 1/K Sum_(i=1)^K (1-ACCi)
data.BER = (1/K)*(3-data.Droplet.accuracy-data.Ice.accuracy-data.Artifact.accuracy);
%K: number of classes
%Nk: number of samples in class k
%yn: the true label of n
%y^n: predicted class of n

%(data.Ice.accuracy + data.Droplet.accuracy + data.Artifact.accuracy)/3;
% end
data.Artifact.FDR = sum(M(1,2:3))/sum(M(1,:));
data.Droplet.FDR = (M(2,1)+M(2,3))/sum(M(2,:));
data.Ice.FDR = sum(M(3,1:2))/sum(M(3,:));
data.FDR = (sum(M(1,2:3))+ (M(2,1)+M(2,3))+sum(M(3,1:2)))/sum(sum(M));
data.accuracyParticles = (M(1,1)+M(2,2))/sum(sum(M(1:3,1:2)));

end