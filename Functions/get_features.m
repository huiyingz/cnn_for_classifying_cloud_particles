%Input: struct with field prtclID
%Output: same struct with additional field of x, y and z position
function data = get_features(data)
%data.rx has to include the field prtclID
if ~isfield(data,'prtclID')
    warning('No field prtclID in struct')
    return
end
l = length(data.prtclID);
for cnt = 1:l
    a = findstr(data.prtclID{cnt},'_');
    xpos = data.prtclID{cnt}(a(1)+1:a(2)-1);
    ypos = data.prtclID{cnt}(a(2)+1:a(3)-1);
    zpos = data.prtclID{cnt}(a(3)+1:a(4)-1);
    size = data.prtclID{cnt}(a(4)+1:length(data.prtclID{cnt}));
    data.xpos(cnt) = str2num(xpos);
    data.ypos(cnt) = str2num(ypos);
    data.zpos(cnt) = str2num(zpos);
    data.size(cnt) = str2num(size);
    clear a b
end

end