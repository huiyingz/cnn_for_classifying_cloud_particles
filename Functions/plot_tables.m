%Input: struct with fields of different fields containing fields of
%accuracy, Artifact.accuracy, Artifact.FDR etc.
%output: Boxplot and scatterplot
function [ax1,ax2,ax3,ax4] = plot_tables(data)
%data has to contain fields with methods
%%note: means of dcnn should be medians!
ms =500;
fs = 15;


%Avoid no output of function
ax1 = 1;
ax2 = 1;
ax3 = 1;
ax4 = 1;

%data has to contain fields with methods

method = fieldnames(data);
% n_metric = 7; %number of metrics
n_metric_plot = 4;
n_method = length(method); %number of used methods
col1 = [.5 1 1];
col3 = [0 .2 .8]; %Blau 3-4
col2 = [.8 0 0]; %Rot 2-3
col6 = [0 .6 .2]; %gr�n
col5 = [.6 0 .8]; %lila 5-2
col4 = [1 .6 .2]; %orange 4-5
markers = {'b*' 'bo' 'bs' 'b<'};
col = {col1,col2,col3,col4,col5,col6}; %colors

ss = 1/(n_method+1);
yline = [0,1]; %lines to separate bins
xline = (1:n_metric_plot);
xline = repmat(xline,2,1);

for cnt = 1:n_method
    y_acc.(method{cnt})(:,1) =  data.(method{cnt}).accuracy;
    y_acc.(method{cnt})(:,2) =  data.(method{cnt}).Artifact.accuracy;
    y_acc.(method{cnt})(:,3) =  data.(method{cnt}).Droplet.accuracy;
    y_acc.(method{cnt})(:,4) =  data.(method{cnt}).Ice.accuracy;
    y_fdr.(method{cnt})(:,1) =  1-data.(method{cnt}).accuracy;
    y_fdr.(method{cnt})(:,2) =  data.(method{cnt}).Artifact.FDR;
    y_fdr.(method{cnt})(:,3) =  data.(method{cnt}).Droplet.FDR;
    y_fdr.(method{cnt})(:,4) =  data.(method{cnt}).Ice.FDR;
end

j= 1;
y_a = [];
y_f = [];

for i = 1:n_metric_plot
    for cnt = 1:n_method
        y_a(:,j) = y_acc.(method{cnt})(:,i);
        y_f(:,j) = y_fdr.(method{cnt})(:,i);
        j = j+1;
    end
    y_a(:,j) = NaN;
    y_f(:,j) = NaN;
    j = j+1;
end


%Scatterplot
figure(1)
%FDR
ax3 = subplot(2,1,1);
for cnt = 1:n_method
    datapoints = size(y_fdr.(method{cnt}),1);
    x.(method{cnt}) = repmat(ss*cnt:1:n_metric_plot-0.01,1,datapoints);
    x.(method{cnt}) = sort(x.(method{cnt}));
    y.(method{cnt}) = reshape(y_fdr.(method{cnt}),[n_metric_plot*datapoints,1]);
    scatter(x.(method{cnt}),y.(method{cnt}),ms,markers{cnt})
    hold on
end
plot(xline(:,1),yline,'k',xline(:,2),yline,'k',...
    xline(:,3),yline,'k',xline(:,4),yline,'k');
set(gca,'xtick',[0.5 1.5 2.5 3.5],'xticklabels',{'Overall','Artifact','Water','Ice'},'FontSize',fs);
set(gca,'XMinorTick','off','FontName','Arial');
xlim([0 4])
ylabel('FDR (%)','FontSize',fs)

%Accuracy
ax4 = subplot(2,1,2);
for cnt = 1:n_method
    datapoints = size(y_acc.(method{cnt}),1);
    x.(method{cnt}) = repmat(ss*cnt:1:n_metric_plot-0.01,1,datapoints);
    x.(method{cnt}) = sort(x.(method{cnt}));
    y.(method{cnt}) = reshape(y_acc.(method{cnt}),[n_metric_plot*datapoints,1]);
    scatter(x.(method{cnt}),y.(method{cnt}),ms,markers{cnt})
    hold on
end
plot(xline(:,1),yline,'k',xline(:,2),yline,'k',...
    xline(:,3),yline,'k',xline(:,4),yline,'k');
set(gca,'xtick',[0.5 1.5 2.5 3.5],'xticklabels',{'Overall','Artifact','Water','Ice'},'FontSize',fs);
set(gca,'XMinorTick','off');
ylabel('Accuracy (%)','FontSize',fs)
xlim([0 4])



if size(y_f,1) > 3
%Boxplot
figure(2)
ax1 =subplot(2,1,1);
h=boxplot(y_f);%,'labels', {'','Overall','','','','Artifact','','','','Water','','','','Ice','',''})
set(h,{'linew'},{2})
set(gca,'XTickLabel',{' '},'xtick',[],'FontSize',fs,'FontName','Arial')
ylabel('FDR (%)','FontSize',fs)
a = get(get(gca,'children'),'children');   % Get the handles of all the objects
t = get(a,'tag');   % List the names of all the objects 
p = strcmp(t,'Box');
p = find(p==1);
c = 1;
for i = 1:n_metric_plot
    for j = 1:n_method+1
        set(a(p(c)), 'Color', col{j});   % Set the color of the first box to green
        c=c+1;
    end
end
clear a t p c i j


ax2 = subplot(2,1,2);
h=boxplot(y_a);%,'labels', {'','Overall','','','','Artifact','','','','Water','','','','Ice','',''})
set(h,{'linew'},{2})
set(gca,'XTickLabel',{' '},'xtick',[],'FontSize',fs)
ylabel('Accuracy (%)','FontSize',fs)
a = get(get(gca,'children'),'children');   % Get the handles of all the objects
t = get(a,'tag');   % List the names of all the objects 
p = strcmp(t,'Box');
p = find(p==1);
c = 1;
for i = 1:n_metric_plot
    for j = 1:n_method+1
        set(a(p(c)), 'Color', col{j});   % Set the color of the first box to green
        c=c+1;
    end
end
clear a t p c i j
annotation('textbox', [0.195,0.03,0.07,0.06],'String', 'Overall','LineStyle','none','FontSize',fs);
annotation('textbox', [0.387,0.03,0.07,0.06],'String', 'Artifact','LineStyle','none','FontSize',fs);
annotation('textbox', [0.582,0.03,0.07,0.06],'String', 'Water','LineStyle','none','FontSize',fs);
annotation('textbox', [0.776,0.03,0.07,0.06],'String', 'Ice','LineStyle','none','FontSize',fs);

%%Legend
hold on
h = zeros(4,1);
h(1) = plot(NaN,NaN,'s','Color',col4,'LineWidth',2);
h(2) = plot(NaN,NaN,'s','Color',col3,'LineWidth',2);
h(3) = plot(NaN,NaN,'s','Color',col2,'LineWidth',2);
% h(4) = plot(NaN,NaN,'s','Color',col2,'LineWidth',2);
% leg = {'Decision tree','SVM','CNN','fine-tuned CNN'};
leg = {'Decision tree','SVM','CNN'};
[h,icons] = legend(leg,'Location','bestoutside');
% Find the 'line' objects
icons = findobj(icons,'Type','line');
icons = findobj(icons,'Marker','none','-xor');
set(icons,'MarkerSize',15);


end

end
