function level = bestPruning(testData,tree)

data = testData;
p = length(tree.PruneAlpha)-1; %number of possible pruning levels

data.Artifact.class = find(data.class == 'Artifact');
data.Droplet.class = find(data.class == 'Particle_round');
data.Ice.class = find(data.class == 'Particle_nubbly');

%%Check every pruning level
for i = 1:p

tree_pred = prune(tree,'level',i);
    
%% Predict with tree
[data.metricnames,~,ib] = intersect(tree_pred.PredictorNames,data.metricnames);
data.metricmat = data.metricmat(:,ib);
data.predicted = predict(tree_pred,data.metricmat);
if iscell(data.predicted)
    data.predicted = categorical(data.predicted);
end
   
data.Artifact.predicted = find(data.predicted == 'Artifact');
data.Artifact.true = intersect(data.Artifact.class,data.Artifact.predicted);
data.Artifact.accuracy = length(data.Artifact.true)/length(data.Artifact.class);

data.Droplet.predicted = find(data.predicted == 'Particle_round');
data.Droplet.true = intersect(data.Droplet.class,data.Droplet.predicted);
data.Droplet.accuracy = length(data.Droplet.true)/length(data.Droplet.class);

data.Ice.predicted = find(data.predicted == 'Particle_nubbly');
data.Ice.true = intersect(data.Ice.class,data.Ice.predicted);
data.Ice.accuracy = length(data.Ice.true)/length(data.Ice.class);

data.accuracy(i) = (length(data.Droplet.true)+length(data.Ice.true)+length(data.Artifact.true))/length(data.class);
end

%% Get bestPruningLevel
[~,level] = max(data.accuracy); 


end